Welcome to the Root Segmentation on Minirhizotron Images (RSMI) repository
==========================================================================
Maintainer: F. Postic
![RSMI logo](https://gitlab.com/rsmi/rsmi.gitlab.io/raw/Dev/docs/Logo1alt.png)
![RSMI principle](https://gitlab.com/rsmi/rsmi.gitlab.io/raw/Dev/docs/Schema1Bis_k.png )


Requirements
------------
This module is implemented in Python 3.4, and requires Scikit-learn and OpenCV 2.49.

**For Ubuntu 14.04 LTS and newer:**
```
sudo apt-get install python3
sudo apt-get install python3-pip
sudo pip install sklearn opencv-python==2.4.9
```


Install
-------
Clone this git:
```
git clone https://gitlab.com/rsmi/rsmi.gitlab.io
```


Unit test
---------
You can perfom unit test:

```
cd rsmi.gitlab.io
python3 test_main.py
```

Usage
-----
Please follow the [short guide](https://rsmi.gitlab.io/examples.html) provided in the [documentation](https://rsmi.gitlab.io/).

License
-------
Arvalis-l'Institut du Végétal