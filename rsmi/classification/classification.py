"""Image Classification module.
"""
from cv2 import convertScaleAbs
from rsmi import utils

class image_classifier:
	"""A class that apply a trained classifier to an image.
	
	Parameters
	----------
	None
	
	Methods
	-------
	predict_proba: 
		method for classifying an image with probabilities (or decision function) as output values.
	predict:
		method for classifying an image into binary classes.

	Attributes
	----------
	image_classified: numpy matrix (uint8)
		the matrix representation of the classified image
	
	"""
	
	def __init__(self):
	
		pass

	def predict_proba(self, image_to_classify_object, trained_classifier_object,class_idx = 1):
		"""Classify an image with probabilities (or decision function) as output values.
		
			Parameters
			----------
			image_to_classify_object: object of class "image_to_classify"
				An instance of a original image loaded
			trained_classifier_object: sklearn instance of a trained classifier
			class_idx: (optional) int (default = 1)int
				target class index. In binary classification, indices are 0 or 1

		"""

		image_with_feature = utils.reshape_filtered_images(image_to_classify_object, sub_sample_array = None) # Create an matrix with the shape expected by the classifier (nSamples x nFeatures)

		if hasattr(trained_classifier_object,'predict_proba'):
		
			image_classified = trained_classifier_object.predict_proba(image_with_feature) # Predict the class (under classification sense) of the input image

			image_classified = image_classified[:,class_idx] 
			
			image_classified = convertScaleAbs(image_classified, alpha=255, beta = 0)
			
		elif hasattr(trained_classifier_object,'decision_function'):
			
			image_classified = trained_classifier_object.decision_function(image_with_feature) # Predict the class (under classification sense) of the input image

			image_classified = image_classified[:,class_idx] 
			
			image_classified = convertScaleAbs(image_classified, alpha=255.0/2.0, beta = 1.0)
			
		else :
			print('Error in image_classifier.predict_proba:',classifier)
			raise ValueError('this classifier has no attribute for probabilites or decision function')
		
		
		rows,cols,channels = image_to_classify_object.image.shape # Get the dimensions of the input image /!\Warning: errors expected in case of grayscale image as input

		image_classified = image_classified.reshape((rows,-1)) # Reshape the predict classes array into the original dimensions of the matrix

		self.image_classified = image_classified # Store the classified image 
		
		
		
	def predict(self, image_to_classify_object, trained_classifier_object, threshold= None, class_idx = 1):
		"""A method for classifying an image into two classes.
		
			Parameters
			----------
			image_to_classify_object: object of class "image_to_classify"
				An instance of a original image loaded
			trained_classifier_object: sklearn instance of a trained classifier
			threshold: (optional) float betwen -1 and 1(default = None)
				Threshold applied on probability membership on the target class
			class_idx: (optional) int (default = 1)
				target class index. In binary classification, indices are 0 or 1

		"""
	
		if threshold is None : # set the default value for the threshold
		
			if hasattr(trained_classifier_object,'predict_proba'):
			
				threshold = 0.5
			
			elif hasattr(trained_classifier_object,'decision_function'):
			
				threshold = 0
			
			else :
				print('Error in image_classifier.predict',classifier)
				raise ValueError('this classifier has no attribute for probabilites or decision function')
	
	
		self.predict_proba(image_to_classify_object, trained_classifier_object,class_idx)
		
		# convert into 0(False) or 255 (True)
		
		self.image_classified[self.image_classified <= threshold*255] = 0
		self.image_classified[self.image_classified > threshold*255] = 255
		
		
		