""" Classification module of rsmi

"""

__version__ = '1.0'

__all__ = ['image_classifier']

__author__ = 'Francois Postic <f.postic@arvalis.fr>'

from .classification import image_classifier
