# univariate_ranking
import numpy

from sklearn.feature_selection import f_classif
from sklearn.feature_selection import chi2

from sklearn.model_selection import train_test_split

from rsmi.utils import  rank_to_dict




def univariate_ranking (datasetObject,  n_folds, score = True):
	"""Feature selection with univariate ranking. Compute Anova, Mutual Information, Chi²
	
	Parameters
	----------
	datasetObject: object of class "dataset"
		An instance of a dataset, must have a 'feature_names' attribute
	score: bool (optional) 
		if True returns scores, if false return normalized scores between 0-1
	n_folds: int
		number of splits
	
	Returns
	-------
	ranks_dict: dict
		dictionary of the score of Anova, Mutual Information, Chi²,with features names
	
	"""
	
	X = datasetObject.data # features array
	
	y = datasetObject.target # target array
	
	feature_names = datasetObject.feature_names
	
	n_features = len(feature_names) # number of features used
	
	ranks_dict = {} # initialize dictionary
	
	F_value = numpy.zeros((n_features,n_folds)) # initialize Anova scores 
	
	chi2_score = numpy.zeros((n_features,n_folds)) # initialize Chi squared scores 
	
	for i in range(0, n_folds):
	
		X_train, X_test, y_train, y_test = train_test_split(X, y, test_size= 1/n_folds)

		F_value[:,i] = f_classif(X_test, y_test)[0]

		chi2_score[:,i] = chi2(X_test, y_test)[0]
	
	
	ranks_dict['ANOVA'] = rank_to_dict(numpy.mean(F_value, axis = 1), feature_names, order=1, score= score) # mean Anova score
	
	ranks_dict['ANOVA_sd'] = rank_to_dict(numpy.std(F_value, axis = 1), feature_names, order=1, score= score) # standard dev of Anova score
	
	ranks_dict['Chi2'] = rank_to_dict(numpy.mean(chi2_score, axis = 1), feature_names, order=1, score= score) # mean Chi squared score
	
	ranks_dict['Chi2_sd'] = rank_to_dict(numpy.std(chi2_score, axis = 1), feature_names, order=1, score= score) # standard dev of Chi squared score

	return ranks_dict
