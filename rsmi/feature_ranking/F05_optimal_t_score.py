#F05_optimal_t_score
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import Binarizer
from sklearn.preprocessing import MinMaxScaler

from sklearn.metrics import fbeta_score
from sklearn.metrics import precision_score

import numpy

def F05_optimal_t_score(classifier, X, y, nfolds, nbins, target_class = 1 ) :
	""" Compute F0.5 score at n values of classification threshold, and find the optimal threshold where F05 is maximum. 
	
	Parameters
	----------
	classifier:
		instance of a sklearn classifier
	X: numpy matrix
		input data
	y: numpy matrix
		target data
	nfolds: int
		number of folds for the cross validation
	nbins: int
		number of bins for computing classification threshold effect on precision
	target_class: int (optional,default = 1)
		column of the target class for predictions based on probabilities
	
	Returns
	-------
	maximum_F05_score: float
		the maximum of the F05 score
	std_at_max_F05_score: float
		the standard deviation of the F05 score over the n folds
	optimal_treshold: float
		the threshold for this maximum F05 score
	
	"""
	beta = 0.5 # beta parameter for F 0.5 score
	
	step = 1/nbins

	thresholds = numpy.arange(0+step/2, 1, step) # ranges for threshold, in a probabilistic range [0,1]

	F05_score_curve = numpy.zeros((nbins,nfolds))
	
	# initialize threshold values tested 
	if hasattr(classifier,"predict_proba"):
			
		pass 
			
	elif hasattr(classifier,"decision_function"):
	
		thresholds = 2*thresholds - 1 # in case of a decision function, range spans over [-1, 1]

	else:
		print(classifier)
		raise ValueError('this classifier has no attribute for probabilites or decision function')

	
	skf = StratifiedKFold(n_splits = nfolds) # generate a stratified K fold splitter

	fold_idx = 0
	
	for train_index, test_index in skf.split(X, y): # a loop on the cross validation folds

	
		# create the train and test samples
		X_train, X_test = X[train_index], X[test_index]
		y_train, y_test = y[train_index], y[test_index]

		classifier.fit(X_train,y_train) # train the classifier on the training set
		
		if hasattr(classifier,"predict_proba"):
			
			y_pred = classifier.predict_proba(X_test) # make prediction using the predict probabilities
			
			y_pred = y_pred[:,target_class] # reduce to the target class column
			
			y_pred =numpy.stack( (y_pred, y_pred), axis=1) # double columns because MinMaxScaler and Binarizer don't support 1d array
	
				
			
		else :

			y_pred = classifier.decision_function(X_test) # make prediction with the decision function

			# Set the range between -1 1
			scaler = MinMaxScaler(copy=True, feature_range=(-1, 1))
			
			y_pred =numpy.stack( (y_pred, y_pred), axis=1) # double columns because MinMaxScaler and Binarizer don't support 1d array

			scaler = MinMaxScaler(copy=True, feature_range=(-1, 1))
			scaler.fit(y_pred)
			y_pred = scaler.transform( y_pred )

		# Compute score for each threshold the optimum threshold for F0.5
		
		i = 0

		for k in thresholds :
			
			binarizer = Binarizer(threshold = k)

			y_bin = binarizer.transform(y_pred)

			y_bin_k = y_bin[:,0]
			
			F05_score_curve[i,fold_idx] = fbeta_score( y_test,y_bin_k , beta, labels=None, pos_label=1, average='binary', sample_weight=None)

			i = i + 1
				
		fold_idx = fold_idx +1
	
	F05_score_curve_mean = numpy.mean(F05_score_curve, axis = 1) # average of the F05 score over the 10 folds
	
	F05_score_curve_std = numpy.std(F05_score_curve, axis = 1) # std of the F05 score over the 10 folds
	
	#Outputs
	maximum_F05_score = numpy.max(F05_score_curve_mean) # return maximum of F05 score
	
	std_at_max_F05_score = F05_score_curve_std[numpy.argmax(F05_score_curve_mean)] # return its standard dev
	
	optimal_treshold = thresholds[numpy.argmax(F05_score_curve_mean)] # return its threshold
	
	return maximum_F05_score, std_at_max_F05_score, optimal_treshold