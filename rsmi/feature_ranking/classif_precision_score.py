#classif_precision_score
from sklearn.model_selection import cross_val_score
from .precision_optimal_t_score import precision_optimal_t_score
import numpy
import time

def classif_precision_score(classif_list, classif_names, X, y, nfolds, features_ranks, nbins = 40  ) :
	"""A method that generates a two-way table for cross-validated precsion scores.
		For each classifier listed , computes a cross-validated on nfolds over the training set (X , y), with a varying number of features used.
		The incrementation of features used follows the list given by features_ranks.
		Compute also the standard deviation of the precision, and the corresponding probability/decision function threshold
	
	Parameters
	----------
	classif_list: list of sklearn classifier
	classif_names: list of str
		Names of the classifiers under test, length = length (classif_list)
	X: numpy matrix
		Data values used for classification, shape : nsamples x nfeatures.
	y: numpy matrix
		target class values, shape : nsamples x 1
	nfolds: int
		number of folds used in the cross-validation process
	features_ranks: list of int
		list of the indices of the features used during the incrementation
	nbins: int (optional, default = 40)
		number of bins that sample the probability/decision function threshold
	
	Returns
	-------
	precision_scores_dictionnary: dict
		dictionary composed of sub-dictionaries that report the scores for each classifier
	"""

	precision_scores_dictionnary = {}
	
	features_ranks = numpy.array(features_ranks)
	
	for k in range(0,len(classif_names)) :
		
		clf = classif_list[k]
		
		print('Compute precision score for: ', classif_names[k])
		
		tested_classifier_dict_mean = {}
		tested_classifier_dict_std = {}
		tested_classifier_dict_thresh = {}
		
		
		t0 = time.time()
		
		for i in range(len(features_ranks)) :
			
			ta = time.time()
			
			nfeatures_used = len(features_ranks) - i

			feature_used_idx = features_ranks <= nfeatures_used
			
			local_X = X[:,feature_used_idx]
			
			mean_precision_score, std_precision_score, Optimal_threshold = precision_optimal_t_score(clf, local_X, y, nfolds, nbins = nbins )
		
			tested_classifier_dict_mean['Top '+str(nfeatures_used)+' features'] = mean_precision_score
			
			tested_classifier_dict_std['Top '+str(nfeatures_used)+' features'] = std_precision_score
			
			tested_classifier_dict_thresh['Top '+str(nfeatures_used)+' features'] = Optimal_threshold
			
			tb = time.time()
			
			print(classif_names[k], ' with ',nfeatures_used,' feature fit in:', tb-ta, ' seconds')

			
		t1 = time.time()
			
		key_mean = 'mean_' + classif_names[k]
		
		key_std = 'std_' + classif_names[k] 
		
		key_thresh = 'tresh_' + classif_names[k] 
		
		precision_scores_dictionnary[key_mean]= tested_classifier_dict_mean
		
		precision_scores_dictionnary[key_std]= tested_classifier_dict_std
		
		precision_scores_dictionnary[key_thresh]= tested_classifier_dict_thresh
		
		print('')
		print(classif_names[k], ' precision score computed in:', t1-t0, ' seconds')
		print('')
		
	return precision_scores_dictionnary