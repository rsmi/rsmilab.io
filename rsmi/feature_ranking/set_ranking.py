# set_ranking
import numpy

from sklearn.linear_model import RandomizedLogisticRegression

from sklearn.model_selection import train_test_split

from rsmi.utils import  rank_to_dict




def set_ranking (datasetObject, n_folds, score = True, C=1):
	"""Feature selection with stability ranking with Randomized Logistic Regression.
	Randomized Logistic Regression provides scores represents how often a feature is selected in the best set.
	
	Parameters
	----------
	datasetObject: object of class "dataset"
		An instance of a dataset, must have a 'feature_names' attribute
	score: bool (optional)
		if True returns scores, if false return normalized scores between 0-1
	n_folds: int
		number of splits
	
	Returns
	-------
	ranks_dict: dict
		dictionary of the score of stability score with features names
	"""
	
	X = datasetObject.data
	
	y = datasetObject.target
	
	feature_names = datasetObject.feature_names
	
	n_features = len(feature_names)
	
	ranks_dict = {}
	
	stability_score = numpy.zeros((n_features,n_folds))
	
	rlogistic = RandomizedLogisticRegression(C, selection_threshold= 0.950, tol=0.001, n_resampling = 200, verbose=True, n_jobs=1)
	
	print('Set ranking, number of folds : ', n_folds   )
	
	for i in range(0, n_folds):
	
		print('Set ranking: fold ', i )
		
		X_train, X_test, y_train, y_test = train_test_split(X, y, test_size= 1/n_folds)
		
		print('    Data splitted ')
		
		rlogistic.fit(X_test, y_test)
		
		print('    Random Logistic Regression applied')
		
		stability_score[:,i] =  rlogistic.scores_
		print('    Scores computed')

	
	ranks_dict['Stability'] = rank_to_dict(numpy.mean(stability_score, axis = 1), feature_names, order=1,score =score)
	
	ranks_dict['Stability_sd'] = rank_to_dict(numpy.std(stability_score, axis = 1), feature_names, order=1,score =score)
	

	return ranks_dict
