#classif_roc_auc_score
from sklearn.model_selection import cross_val_score
import numpy
import time

def classif_roc_auc_score(classif_list, classif_names, X, y, nfolds, features_ranks ) :
	"""A method that generates a two-way table for cross-validated area under the curve (AUC) of false positives vs true positives (ROC curve).
		For each classifier listed , computes a cross-validated on nfolds over the training set (X , y), with a varying number of features used.
		The incrementation of features used follows the list given by features_ranks.
		Compute also the standard deviation of the ROC AUC, and the corresponding probability/decision function threshold
	
	Parameters
	----------
	classif_list: list of sklearn classifier
	classif_names: list of str
		Names of the classifiers under test, length = length (classif_list)
	X: numpy matrix
		Data values used for classification, shape : nsamples x nfeatures.
	y: numpy matrix
		target class values, shape : nsamples x 1
	nfolds: int
		number of folds used in the cross-validation process
	features_ranks: list of int
		list of the indices of the features used during the incrementation
	nbins: int (optional, default = 40)
		number of bins that sample the probability/decision function threshold

	Returns
	-------
	AUC_scores_dictionnary: dict
		a dictionary composed of sub-dictionaries that report the scores for each classifier
	"""

	AUC_scores_dictionnary = {}
	
	features_ranks = numpy.array(features_ranks)
	
	for k in range(0,len(classif_names)) :
		
		clf = classif_list[k]
		print('Compute ROC AUC score for: ', classif_names[k])
		tested_classifier_dict_mean = {}
		tested_classifier_dict_std = {}
		
		t0 = time.time()
		
		for i in range(len(features_ranks)) :
			
			ta = time.time()
			
			nfeatures_used = len(features_ranks) - i

			feature_used_idx = features_ranks <= nfeatures_used
			
			local_X = X[:,feature_used_idx]
			
			scores = cross_val_score(clf, local_X, y, scoring= 'roc_auc', cv=nfolds, n_jobs =-1)
			
			mean_roc_auc = numpy.mean(scores)
			
			std_roc_auc = numpy.std(scores)
		
			
			tested_classifier_dict_mean['Top '+str(nfeatures_used)+' features'] = mean_roc_auc
			
			tested_classifier_dict_std['Top '+str(nfeatures_used)+' features'] = std_roc_auc
			
			tb = time.time()
			
			print(classif_names[k], ' with ',nfeatures_used,' feature fit in:', tb-ta, ' seconds')

			
		t1 = time.time()
			
		key_mean = 	classif_names[k] + '_mean'
		
		key_std = 	classif_names[k] + '_std'
		
		AUC_scores_dictionnary[key_mean]= tested_classifier_dict_mean
		
		AUC_scores_dictionnary[key_std]= tested_classifier_dict_std
		
		print('')
		print(classif_names[k], ' AUC score computed in:', t1-t0, ' seconds')
		print('')
		
	return AUC_scores_dictionnary