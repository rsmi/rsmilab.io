#isotrope_kernel_based
from rsmi import utils
import cv2
import numpy

def minimum(image,ksize):
	"""Method that computes the minimum over a ksize sized kernel.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Returns
	-------
	min_image: uint8 numpy matrix
		product of the original image with the Minimum filter
	"""
	kernel = numpy.ones((ksize,ksize),'uint8') # construct the kernel

	min_image = cv2.erode(image, kernel) # erode (equivalent to min filter)

	return cv2.convertScaleAbs(min_image)	

def maximum(image,ksize):
	"""Method that computes the maximum over a ksize sized kernel.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Returns
	-------
	max_image: uint8 numpy matrix
		product of the original image with the Maximum filter
	"""
	kernel = numpy.ones((ksize,ksize),'uint8') # construct the kernel

	max_image = cv2.dilate(image, kernel) # apply a dilatation (equivalent to max filter)

	return cv2.convertScaleAbs(max_image)

def mean(image,ksize):
	"""Method that computes the mean over a ksize sized kernel.

	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Returns
	-------
	mu: uint8 numpy matrix
		product of the original image with the Mean filter
	"""
	kernel = (ksize,ksize) #ksize of kernel

	mu = cv2.blur(image, kernel)# Apply mean filter (blurred image)

	return cv2.convertScaleAbs(mu)

def variance(image,ksize):
	"""Method that computes the variance over a ksize sized kernel.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Returns
	-------
	sigma_32f: uint8 numpy matrix
		product of the original image with the Variance filter
	"""
	kernel = (ksize,ksize) #ksize of kernel

	image_32f = utils.im2double(image) # coarse conversion in float

	mu_32f = cv2.blur(image_32f, kernel)# Apply mean filter (blurred image)

	mu2_32f = cv2.blur(cv2.multiply(image_32f,image_32f), kernel) # Squared image then filtered by mean filter

	sigma_32f = mu2_32f - cv2.multiply(mu_32f,mu_32f) # Compute variance as Var = E(X^2) - E(X)^2
	
	cv2.normalize(sigma_32f,sigma_32f,0,255,cv2.NORM_MINMAX)
	
	return cv2.convertScaleAbs(sigma_32f)

def sd(image,ksize):
	"""Method that computes the standard deviation over a ksize sized kernel.

	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Returns
	-------
	sd_32f: uint8 numpy matrix
		product of the original image with the Standard Deviation filter
	"""
	kernel = (ksize,ksize) #ksize of kernel

	image_32f = utils.im2double(image) # coarse conversion in float

	mu_32f = cv2.blur(image_32f, kernel)# Apply mean filter (blurred image)
	
	mu2_32f = cv2.blur(numpy.power(image_32f,2), kernel) # Squared image then filtered by mean filter
	
	sigma_32f = mu2_32f - numpy.power(mu_32f,2) # Compute variance as Var = E(X^2) - E(X)^2
	
	sd_32f = numpy.sqrt(numpy.around(sigma_32f, decimals=7)) # rounding used in order to prevent "sqrt(-0.00000001)"
	
	cv2.normalize(sd_32f,sd_32f,0,255,cv2.NORM_MINMAX)
	
	return cv2.convertScaleAbs(sd_32f)
