#directionnal_kernel_based
from rsmi import utils
import numpy
import cv2

def sobel(image,ksize):
	"""Method that computes directional Sobel in x direction and y direction on a kernel of ksize (must be odd).
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Output:
	sobel_64f: uint8 numpy matrix
		product of the original image with the Sobel filter
	"""

	ddepth = cv2.CV_64F # destination bit depth

	kernel_Gx = utils.kernel_generator.sobel_x(ksize) # kernel for a horizontal sobel
	
	kernel_Gy = utils.kernel_generator.sobel_y(ksize) # kernel for a vertical sobel
	
	sobelx_64f = cv2.filter2D(image, ddepth ,kernel_Gx) # horizontal sobel
	
	sobely_64f = cv2.filter2D(image, ddepth ,kernel_Gy) # vertical sobel

	sobel_64f = numpy.sqrt(numpy.square(sobelx_64f)+ numpy.square(sobely_64f)) # full sobel computed as sqrt(x²+y²)

	return cv2.convertScaleAbs(sobel_64f) # return a 8 bit unsigned numpy matrix

def prewitt(image,ksize):
	"""Method that computes directional Prewitt in x direction and y direction on a kernel of ksize (must be odd).

	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Returns
	-------
	prewitt_64f: uint8 numpy matrix
		product of the original image with the Prewitt filter
	"""
	
	ddepth = cv2.CV_64F # destination bit depth

	kernel_Gx = utils.kernel_generator.prewitt_x(ksize) # kernel for a horizontal prewitt
	
	kernel_Gy = utils.kernel_generator.prewitt_y(ksize) # kernel for a vertical prewitt
	
	prewittx_64f = cv2.filter2D(image, ddepth ,kernel_Gx) # horizontal prewitt
	
	prewitty_64f = cv2.filter2D(image, ddepth ,kernel_Gy) # vertical prewitt

	prewitt_64f = numpy.sqrt(numpy.square(prewittx_64f)+ numpy.square(prewitty_64f)) # full prewitt computed as sqrt(x²+y²)

	return cv2.convertScaleAbs(prewitt_64f) # return a 8 bit unsigned numpy matrix
	
	
def scharr( image, ksize):
	"""Method that computes directional Scharr in x direction and y direction on a kernel of ksize (must be odd).
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Returns
	-------
	scharr_64f: uint8 numpy matrix
		product of the original image with the Scharr filter
	"""

	scharrx_64f = cv2.Scharr(image, cv2.CV_64F ,1,0, ksize) # horizontal scharr

	scharry_64f = cv2.Scharr(image, cv2.CV_64F ,0,1, ksize) # vertical scharr

	scharr_64f = numpy.sqrt(numpy.square(scharrx_64f)+ numpy.square(scharry_64f)) # full scharr computed as sqrt(x²+y²)

	return  cv2.convertScaleAbs(scharr_64f) # return a 8 bit unsigned numpy matrix

def laplacian( image, ksize):
	"""Method that computes directional laplacian in x direction and y direction on a kernel of ksize (must be odd).
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
	ksize: int
		width (or height) of a square kernel used in computation

	Returns
	-------
	laplacian_64f: uint8 numpy matrix
		product of the original image with the laplacian filter
	"""
	
	ddepth = cv2.CV_64F # destination bit depth
	
	kernel = utils.kernel_generator.laplacian(ksize)

	laplacian_64f = cv2.filter2D(image, ddepth ,kernel) # horizontal prewitt

	return cv2.convertScaleAbs(laplacian_64f) # return a 8 bit unsigned numpy matrix

