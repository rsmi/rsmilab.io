""" filter module of rsmi

"""

__version__ = '1.0'

__all__ = ['Grayscale',
			'R_channel','G_channel','B_channel',
			'H_channel','S_channel','V_channel',
			'Rnorm_channel','Gnorm_channel','Bnorm_channel',
			'Hnorm_channel','Snorm_channel','Vnorm_channel',
			'minimum','maximum','mean','variance','sd',
			'sobel', 'prewitt','laplacian']

__author__ = 'François Postic <f.postic@arvalis.fr>'

from .colorspace_method import Grayscale
from .colorspace_method import R_channel
from .colorspace_method import G_channel
from .colorspace_method import B_channel
from .colorspace_method import H_channel
from .colorspace_method import S_channel
from .colorspace_method import V_channel

from .colorspace_method import Rnorm_channel
from .colorspace_method import Gnorm_channel
from .colorspace_method import Bnorm_channel
from .colorspace_method import Hnorm_channel
from .colorspace_method import Snorm_channel
from .colorspace_method import Vnorm_channel

from .isotrope_kernel_based import minimum
from .isotrope_kernel_based import maximum
from .isotrope_kernel_based import mean
from .isotrope_kernel_based import variance
from .isotrope_kernel_based import sd

from .directionnal_kernel_based import sobel
from .directionnal_kernel_based import prewitt
from .directionnal_kernel_based import scharr
from .directionnal_kernel_based import laplacian