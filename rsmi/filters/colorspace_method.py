#colorspace_method
import cv2
import numpy


#----- Standard colorspaces----#

def Grayscale(image):
	"""Method that computes the Grayscale of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	gray_image: uint8 numpy matrix
		grayscale conversion of an image
	"""
	
	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale

	return  cv2.convertScaleAbs(gray_image)

def R_channel(image):
	"""Method that computes the Red channel of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	r: uint8 numpy matrix
		red channel of an image
	"""
	b, g, r = cv2.split(image) # split channels

	return  cv2.convertScaleAbs(r)

def G_channel(image):
	"""Method that computes the Green channelof an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	g: uint8 numpy matrix
		green channel of an image
	"""
	b, g, r = cv2.split(image) # split channels

	return  cv2.convertScaleAbs(g)

def B_channel(image):
	"""Method that computes the Blue channel of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	b: uint8 numpy matrix
		blue channel of an image
	"""
	b, g, r = cv2.split(image) # split channels

	return  cv2.convertScaleAbs(b)

def H_channel(image):
	"""Method that computes the Hue (0-255) channel from HSV color space from an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	h: uint8 numpy matrix
		hue of an image
	"""
	hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) # convert into HSV colorspace

	h, s, v = cv2.split(hsv_image) # split channels

	cv2.normalize(h,h,0,255,cv2.NORM_MINMAX) #normalize with a range of 0-255, not 0-179

	return  cv2.convertScaleAbs(h)

def S_channel(image):
	"""Method that computes the Saturation channel from HSV color space from an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	s: uint8 numpy matrix
		saturation of an image
	"""
	hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) # convert into HSV colorspace

	h, s, v = cv2.split(hsv_image) # split channels

	return  cv2.convertScaleAbs(s)

def V_channel(image):
	"""Method that computes the Value channel from HSV color space from an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	v: uint8 numpy matrix
		Brightness ("value" in HSV) of an image
	"""
	hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) # convert into HSV colorspace

	h, s, v = cv2.split(hsv_image) # split channels

	return  cv2.convertScaleAbs(v)


#----- Normalization-based colorspaces----#

def Rnorm_channel(image):
	"""Method that computes the Normalized Red channel of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image
		
	Returns
	-------
	Rnorm: uint8 numpy matrix
		normalized red channel of an image
	"""
	r = R_channel(image)

	g = G_channel(image)

	b = B_channel(image)

	denominator = r.astype('float32') + g.astype('float32') + b.astype('float32')

	Rnorm = numpy.divide(r.astype('float32'),denominator)

	cv2.normalize(Rnorm,Rnorm,0,255,cv2.NORM_MINMAX)
	
	return  cv2.convertScaleAbs(Rnorm)

def Gnorm_channel(image):
	"""Method that computes the Normalized Green channel of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	Gnorm: uint8 numpy matrix
		normalized green channel of an image
	"""
	r = R_channel(image)

	g = G_channel(image)

	b = B_channel(image)

	denominator = r.astype('float32') + g.astype('float32') + b.astype('float32')

	Gnorm = numpy.divide(g.astype('float32'),denominator)

	cv2.normalize(Gnorm,Gnorm,0,255,cv2.NORM_MINMAX)
	
	return  cv2.convertScaleAbs(Gnorm)
	
def Bnorm_channel(image):
	"""Method that computes the Normalized Blue channel of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	Bnorm: uint8 numpy matrix
		normalized blue channel of an image
	"""
	r = R_channel(image)

	g = G_channel(image)

	b = B_channel(image)

	denominator = r.astype('float32') + g.astype('float32') + b.astype('float32')

	Bnorm = numpy.divide(b.astype('float32'),denominator)

	cv2.normalize(Bnorm,Bnorm,0,255,cv2.NORM_MINMAX)
	
	return  cv2.convertScaleAbs(Bnorm)
		
def Hnorm_channel(image):
	"""Method that computes the Normalized Hue channel of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	Hnorm: uint8 numpy matrix
		normalized hue of an image
	"""
	h = H_channel(image)

	s = S_channel(image)

	v = V_channel(image)

	denominator = h.astype('float32') + h.astype('float32') + v.astype('float32')

	Hnorm = numpy.divide(h.astype('float32'),denominator)

	cv2.normalize(Hnorm,Hnorm,0,255,cv2.NORM_MINMAX)
	
	return  cv2.convertScaleAbs(Hnorm)

def Snorm_channel(image):
	"""Method that computes the Normalized Hue channel of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	Snorm: uint8 numpy matrix
		normalized saturation of an image
	"""
	h = H_channel(image)

	s = S_channel(image)

	v = V_channel(image)

	denominator = h.astype('float32') + h.astype('float32') + v.astype('float32')

	Snorm = numpy.divide(s.astype('float32'),denominator)

	cv2.normalize(Snorm,Snorm,0,255,cv2.NORM_MINMAX)
	
	return  cv2.convertScaleAbs(Snorm)
	
def Vnorm_channel(image):
	"""Method that computes the Normalized Hue channel of an RGB image.
	
	Parameters
	----------
	image: numpy matrix
		matrix representation of the image

	Returns
	-------
	Vnorm: uint8 numpy matrix
		normalized brightness/value of an image
	"""
	h = H_channel(image)

	s = S_channel(image)

	v = V_channel(image)

	denominator = h.astype('float32') + h.astype('float32') + v.astype('float32')
	
	Vnorm = numpy.divide(v.astype('float32'),denominator)

	cv2.normalize(Vnorm,Vnorm,0,255,cv2.NORM_MINMAX)
	
	return  cv2.convertScaleAbs(Vnorm)


