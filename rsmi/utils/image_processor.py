#image_processor
import numpy

def im2double(im):
	"""Convert an image into a float32 image.
	
	Parameters
	----------
	im: numpy matrix,
		matrix representation of a image
	
	Returns
	-------
	numpy matrix (float32),
		matrix representation of a image (same shape as input)

	"""

	min_val = numpy.min(im.ravel())

	max_val = numpy.max(im.ravel())

	out = (im.astype('float32') - min_val) / (max_val - min_val)

	return out