#feature_assembler
import numpy

def bindFunctionSubstr(colorspace1,colorspace2):
	"""Method that creates new methods defined by the subtraction of 2 color spaces

	Parameters
	----------
	colorspace1: method
		a method for creating color space transformation of an image
	colorspace2: method
		a method for creating color space transformation of an image
	
	Returns
	-------
	a method that create a composition (subtraction) of 2 color spaces
	"""
	
	def func1(self,image) : #this is the new function that will be created
	
		col1 = colorspace1(self,image) # the first method that compute a color space
		
		col2 = colorspace1(self,image) # the second method that compute a color space
		
		substr = numpy.substract(col1.astype('float'), col2.astype('float')) # subtract of 2 color spaces in float
		
		return cv2.convertScaleAbs(substr) # convert it into a unit8
		
	newname = colorspace1.__name__[0]+'_Substr_'+colorspace2.__name__[0] # create a name based on the 2 color spaces
	
	func1.__name__= newname # change the name of the newly created function/method
	
	return func1


def bindFunctionAdd(colorspace1,colorspace2):
	"""Method that creates new methods defined by the Add of 2 color spaces

	Parameters
	----------
	colorspace1: method
		a method for creating color space transformation of an image
	colorspace2: method
		a method for creating color space transformation of an image
	
	Returns
	-------
	-a method that create a composition (addition) of 2 color spaces
	"""
	
	def func1(self,image) : #this is the new function that will be created
	
		col1 = colorspace1(self,image) # the first method that compute a color space
		
		col2 = colorspace1(self,image) # the second method that compute a color space
		
		addition = numpy.add(col1.astype('float'), col2.astype('float')) # add of 2 color spaces in float
		
		return cv2.convertScaleAbs(addition) # convert it into a unit8
		
	newname = colorspace1.__name__[0]+'_Add_'+colorspace2.__name__[0] # create a name based on the 2 color spaces
	
	func1.__name__= newname # change the name of the newly created function/method
	
	return func1