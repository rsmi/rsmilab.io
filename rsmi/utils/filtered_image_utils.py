#filtered_image_utils
import numpy

def reshape_filtered_images(image_to_classify_object, sub_sample_array = None):
	"""A method for reshaping matrix shaped filtered images in a array shaped images.
	Handle subsampling when sub_sample_array argument is provided.
	Warning : doesn't manage case when nFeatures == 0 !

	Parameters
	----------
	image_to_classify_object: object of image_to_classify_object class
		Instance of an image including its features
	sub_sample_array: numpy matrix (optional, default = None)
		a 'nSamples x 2'-shaped numpy matrix of the x and y pixel positions of samples drawn from the image (top left of the image is [0, 0])
	
	Returns
	-------
	features_stack: numpy matrix
		numpy matrix 'nSamples x nFeatures'-shaped. This is the format required for classification
	"""

	if sub_sample_array is None: # Case where no optional argument as provided (reshape a whole image with feature)
	
		flag_first = 0 #flag for the first array use at initialization of the feature column stack
		
		
		for i in  image_to_classify_object.features_name :
		
			feature_ID = 'feature_'+str(i) # name of the attribute that contain the filterd image (ex: feature_Grayscale)
		
			if flag_first<1 : # very first feature case 
				
				features_stack = getattr(image_to_classify_object, feature_ID) #initialize the feature stack, with the first feature image reshaped from a matrix into a array
				
				features_stack = features_stack.reshape((-1,1))

				flag_first += 1 # lift (or down) the flag
			
			else :
				
				next_feature_to_stack  = getattr(image_to_classify_object, feature_ID) # get the next feature and reshape it into a array
				
				next_feature_to_stack = next_feature_to_stack.reshape((-1,1))
				
				features_stack = numpy.column_stack((features_stack,next_feature_to_stack)) # append it to the numpy column shaped feature_stack
				
		
	else : # Case where a optionnal argument was provided (reshape a subsample of the image with feature)
			
		flag_first = 0 #flag for the first array use at initialization of the feature column stack
		
		for i in  image_to_classify_object.features_name :
		
			feature_ID = 'feature_'+str(i) # name of the attribute that contain the filterd image (ex: feature_Grayscale)
		
			if flag_first<1 : # very first feature case 
				
				features_stack = getattr(image_to_classify_object,feature_ID)
				
				features_stack = features_stack[sub_sample_array[:,0],sub_sample_array[:,1]] #initialize the feature stack, with the first feature image reshaped from a matrix into a array
				
				flag_first += 1 # lift (or down) the flag
			
			else :
				
				next_feature_to_stack  = getattr(image_to_classify_object,feature_ID)
				
				next_feature_to_stack = next_feature_to_stack[sub_sample_array[:,0],sub_sample_array[:,1]] # get the next feature and reshape it into a array
				
				features_stack = numpy.column_stack((features_stack,next_feature_to_stack)) # append it to the numpy column shaped feature_stack

			
	return features_stack


					
		
		