#module_scanner

def get_method_list(module):
	""" A method that get a list of available methods specifically defined by the module.
	
	Parameters
	----------
	module:
		module previously imported
	
	Returns
	-------
	method_list: list of str
		list with defined methods in the module.
	"""
	method_list = []
	
	for func in dir(module):
		
		if callable(getattr(module,func)):
			
			method_list.append(func)

	return method_list