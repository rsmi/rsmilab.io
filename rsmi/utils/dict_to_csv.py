def dict_to_csv(filename,dict,delimiter = ';' ):
	"""Writes a two-way table in a CSV file, from a dictionary of sub-dictionaries
	Use it to write scores computes during at each selection step.

	Parameters
	----------
	filename: str
		path of the outputting csv file
	dict: dict
		dictionary of the scores
	delimiter: str (optional, default = ';')
		csv delimiter
		
	Returns
	-------
	None
	"""

	methods = [] #list of the ranking method, actually the lvl 0 keys
	feature_names = [] #list of the feature names, actually the lvl 1 keys
	
	#Initialize the list of ranking methods from the dict
	for rankmethod in dict:
		methods.append(rankmethod)

	#Initialize the list of feature names from the dict
	for sublabel in dict[methods[0]]:
		feature_names.append(sublabel)

    
	f = open(filename,'w', newline="\r\n")	 # create the handle of the new file 

	format_headers  = delimiter + '%s'
	f.write( (format_headers % delimiter.join(methods))) # print the headers
	
	format_lines  ='%s' +delimiter +'%s'
	for name in feature_names:

		f.write('\n')#go to the next line
		f.write((format_lines % (name, delimiter.join(map(str,[dict[method][name] for method in methods]))))) # print values

	f.close()