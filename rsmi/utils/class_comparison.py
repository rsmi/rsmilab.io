import numpy

def  class_comparison(image_ref, image_test):
	"""Print the difference between two binarized image.
	Plot true negatives in black, true positive in green, false negative in red and false positive in blue.

	Parameters
	----------
	image_ref: numpy matrix
		matrix representation of the ground truth image
	image_test: numpy matrix
		matrix representation of the image under test
	
	Returns
	-------
	numpy matrix
		matrix representation of the diff, r g b channels
	"""
	
	r, g, b = image_ref[:,:,0], image_ref[:,:,1], image_ref[:,:,2]

	image_ref = numpy.zeros( image_ref.shape[0:2] )
	image_ref = 0.2989 * r + 0.5870 * g + 0.1140 * b


	#initalize a black image
	res_r = numpy.zeros( image_ref.shape )
	res_g = numpy.zeros( image_ref.shape )
	res_b = numpy.zeros( image_ref.shape )

	res_bgr = numpy.zeros(image_ref.shape+(3,), 'uint8')

	# threshold 8 bit images
	image_ref = numpy.round(image_ref/255)

	image_test = numpy.round(image_test/255)

	
	
	diff = image_ref + 2 * image_test


	# true negatives
	mask_k = (diff<1)
	
	res_g[mask_k] = 100
	res_bgr[:,:,2] = res_bgr[:,:,2] + res_g
	res_g[mask_k] = 100
	res_bgr[:,:,1] = res_bgr[:,:,1] + res_g
	res_g[mask_k] = 100
	res_bgr[:,:,0] = res_bgr[:,:,0] + res_g
	

	#true positive in green (a dark green)
	mask_g = ( diff > 2)
	
	res_g[mask_g] = 34
	res_bgr[:,:,2] = res_bgr[:,:,2] + res_g
	res_g[mask_g] = 177
	res_bgr[:,:,1] = res_bgr[:,:,1] + res_g
	res_g[mask_g] = 72
	res_bgr[:,:,0] = res_bgr[:,:,0] + res_g
	
	
	#false negative in red (a less flashy red)
	mask_r = ( (diff > 0) & (diff <= 1))
	
	res_r[mask_r] = 237
	res_bgr[:,:,2] = res_bgr[:,:,2]+ res_r
	res_r[mask_r] = 28
	res_bgr[:,:,1] = res_bgr[:,:,1] + res_r
	res_r[mask_r] = 36
	res_bgr[:,:,0] = res_bgr[:,:,0] + res_r

	#false positive in blue.
	mask_b = ( (diff > 1) & (diff <= 2))
	
	res_b[mask_b] = 63
	res_bgr[:,:,2] = res_bgr[:,:,2] + res_b
	res_b[mask_b] = 72
	res_bgr[:,:,1] = res_bgr[:,:,1] + res_b
	res_b[mask_b] = 204
	res_bgr[:,:,0] = res_bgr[:,:,0] + res_b


	TN = numpy.sum(mask_k) # true negatives count
	TP = numpy.sum(mask_g) # true positive count
	FN =numpy.sum(mask_r) # false negative count
	FP =numpy.sum(mask_b) # false positive count
	
	area_truth = TP+FN # groundtruth sum of of pixels on this image
	area_pred = TP+FP # predicted area
	
	return res_bgr.astype('uint8'), [TN,TP,FN,FP], [area_truth,area_pred]
	
	