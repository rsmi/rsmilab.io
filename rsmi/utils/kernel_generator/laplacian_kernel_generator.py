#laplacian_kernel_generator
import numpy


def laplacian_kernel_generator(ksize):
	"""Method that creates a simple laplacian kernel for any kernel size  (odd).
	
	Parameters
	----------
	ksize: int
		kernel size (square kernel)
	
	Returns
	-------
	kernel: numpy array (shape ksize x ksize)
		the laplacian kernel

	"""

	if ksize%2 :
	
		kernel = numpy.ones((ksize,ksize))
		
		i0 = numpy.int((ksize-1)/2)
		
		j0 = numpy.int((ksize-1)/2)
		
		kernel[i0,j0] = 1-(ksize*ksize)
	
		return kernel
		
	else:
	
		raise ValueError('laplacian_kernel_generator : specified kernel size is pair, should be odd and >1')

	
	