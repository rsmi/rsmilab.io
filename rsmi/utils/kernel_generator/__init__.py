""" kernel generator module of rsmi

"""

__version__ = '1.0'

__all__ = ['kernel_generator']

__author__ = 'François Postic <f.postic@arvalis.fr>'

class kernel_generator():
	
	@staticmethod
	def sobel_x(ksize):
		from .sobel_kernel_generator import sobel_Gx_kernel_generator
		
		return sobel_Gx_kernel_generator(ksize)
		
	@staticmethod
	def sobel_y(ksize):
		from .sobel_kernel_generator import sobel_Gy_kernel_generator
			
		return sobel_Gy_kernel_generator(ksize)
		
	@staticmethod
	def prewitt_x(ksize):
		from .prewitt_kernel_generator import prewitt_Gx_kernel_generator
		
		return prewitt_Gx_kernel_generator(ksize)
		
	@staticmethod
	def prewitt_y(ksize):
		from .prewitt_kernel_generator import prewitt_Gy_kernel_generator
			
		return prewitt_Gy_kernel_generator(ksize)
		
	@staticmethod
	def laplacian(ksize):
	
		from.laplacian_kernel_generator import laplacian_kernel_generator
		
		return laplacian_kernel_generator(ksize)