#sobel_kernel_generator
import numpy


def sobel_Gx_kernel_generator(ksize):
	"""Method that creates a Sobel kernel for any kernel size (odd) in the x-direction.
	
	Parameters
	----------
	ksize: int
		kernel size (square kernel)
	
	Returns
	-------
	kernel: numpy array (shape ksize x ksize)
		the Sobel kernel in the x-direction

	"""
	
	if ksize%2 :

		kernel = numpy.zeros((ksize,ksize),numpy.float32) # initialize a kernel
		
		i0 = (ksize-1)/2
		
		j0 = (ksize-1)/2
		
		for i in range(0,ksize):
		
			for j in range(0,ksize):

				shifted_i = i - i0
				
				shifted_j = j - j0
				
				if ( (shifted_i ==0) & (shifted_j == 0) ) :
				
					kernel[j,i] = 0
				
				else :
					kernel[j,i] = (shifted_i)/((shifted_i*shifted_i)+(shifted_j*shifted_j))

		return kernel
			
	else:
	
		raise ValueError('sobel_kernel_generator : specified kernel size is pair, should be odd and >1')
	

	
	
def sobel_Gy_kernel_generator(ksize):
	"""Method that creates a Sobel kernel for any kernel size (odd) in the y-direction.
	
	Parameters
	----------
	ksize: int
		kernel size (square kernel)
	
	Returns
	-------
	kernel: numpy array (shape ksize x ksize)
		the Sobel kernel in the x-direction

	"""

	if ksize%2 :

		kernel = numpy.zeros((ksize,ksize),numpy.float32) # initialize a kernel
		
		i0 = (ksize-1)/2
		
		j0 = (ksize-1)/2
		
		for i in range(0,ksize):
		
			for j in range(0,ksize):

				shifted_i = i - i0
				
				shifted_j = j - j0
				
				if ( (shifted_i ==0) & (shifted_j == 0) ) :
				
					kernel[j,i] = 0
				
				else :
					kernel[j,i] = (shifted_j)/((shifted_i*shifted_i)+(shifted_j*shifted_j))
		
		return kernel
					
	else:
	
		raise ValueError('sobel_kernel_generator : specified kernel size is pair, should be odd and >1')
	