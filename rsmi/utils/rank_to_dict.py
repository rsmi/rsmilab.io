#rank_to_dict

from sklearn.preprocessing import MinMaxScaler
import numpy

def rank_to_dict(ranks, feature_names, order=1, score=True):
	"""Method for filling a dictionary with cols header  = feature_name and row = rank(score)
	
	Parameters
	----------
	ranks: list of float
		rank position according to a test score, for each feature
	feature_names: list of str
		the name of each feature
	order: int (optional, default = 1)
		sort score in ascending (1) or descending (-1) order
	score: bool (optional, default = True)
		option to convert ranks into original scores (True for scores, False for ranks)
	
	Returns
	-------
	dict
		dictionary with the score of each feature
	
	"""
	
	if score is True :

		ranks = map(lambda x: numpy.round(x, 3), ranks)
		
	else:

		minmax = MinMaxScaler()
		
		ranks = minmax.fit_transform(order*numpy.array([ranks]).T).T[0]
		
		ranks = map(lambda x: round(x, 3), ranks)
	
	return dict(zip(feature_names, ranks ))