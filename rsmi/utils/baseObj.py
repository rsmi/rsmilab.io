#baseObj.py
import types

class ObjConstructor(object):
	"""A class that can import methods from a module. Initialize with a module and a list of desired method for import.
	Require "types" modules
	
	Parameters
	----------
	module: 
		a module containing any method to import 
	method_list: str list (optional, default = None)
		list of the methods to import
		
	Methods
	-------
	removeVariable: 
		remove an attribute of the instance
	
	addMethod:
		add a method to the instance
	
	"""

	def __init__(self, module, method_list = None):

		if method_list is None:

			method_list = [] # avoid using a mutable list as default input

		for i in range(0,len(method_list)):

			func = getattr(module, method_list[i])
			self.addMethod(func)

	@classmethod
	def removeVariable(cls, name):
		return delattr(cls, name)

	@classmethod
	def addMethod(cls, func):
		return setattr(cls, func.__name__, types.MethodType(func, cls))
		