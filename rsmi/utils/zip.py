#zip.py
import zipfile
import numpy
import cv2

class extract_image_from_zip:
	""" A class that extracts an image from a zip archive and load it to the memory.
	Image stored in the 'image_inside' attribute
	
	Parameters
	----------
	filepath: str
		path of the zip file
	
	Attributes
	----------
	extract_image_from_zip.imageinside: numpy matrix
		image stored inside the zip archive
	"""
	
	def __init__(self,filepath):

		zip_archive = zipfile.ZipFile(filepath,'r') # Read the zip Archive 

		zip_content_list = zip_archive.namelist() # Get a list of the content of this archive

		image_data = zip_archive.read(zip_content_list[0]) # Read the data (binary IO) of the first file inside the archive

		self.image_inside = cv2.imdecode(numpy.frombuffer(image_data, numpy.uint8), 1) # Convert it into a numpy array readable by opencv
