""" Utility module of rsmi

"""

__version__ = '1.0'

__all__ = ['extract_image_from_zip',
			'bindFunctionSubstr','bindFunctionAdd','im2double',
			'get_method_list','reshape_filtered_images',
			'kernel_generator',
			'rank_to_dict', 'dict_to_csv',
			'class_comparison','false_overlay']

__author__ = 'François Postic <f.postic@arvalis.fr>'

from .zip import extract_image_from_zip
from .feature_assembler import bindFunctionSubstr
from .feature_assembler import bindFunctionAdd
from .image_processor import im2double
from .module_scanner import get_method_list
from .filtered_image_utils import reshape_filtered_images
from .kernel_generator import kernel_generator
from .rank_to_dict import rank_to_dict
from .dict_to_csv import dict_to_csv
from .class_comparison import class_comparison
from .false_overlay import false_overlay
