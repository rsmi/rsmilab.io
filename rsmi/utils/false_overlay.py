import numpy

def  false_overlay(image_ref, image_test):
	"""Create an transparent overlay the false positives and false negatives of an image.
	Plot false negative in red and false positive in blue.
	
	Parameters
	----------
	image_ref: numpy matrix
		matrix representation of the ground truth image
	image_test: numpy matrix
		matrix representation of the image under test
	
	Returns
	-------
	numpy matrix (uint8)
		matrix representation of the differences, r g b channels
	"""
	
	r, g, b = image_ref[:,:,0], image_ref[:,:,1], image_ref[:,:,2]

	image_ref = numpy.zeros( image_ref.shape[0:2] )
	image_ref = 0.2989 * r + 0.5870 * g + 0.1140 * b


	#initialize a black image

	res_b = numpy.zeros( image_ref.shape )
	res_r = numpy.zeros( image_ref.shape )

	res_bgr = numpy.zeros(image_ref.shape+(4,), 'uint8')

	# threshold 8 bit images
	image_ref = numpy.round(image_ref/255)

	image_test = numpy.round(image_test/255)

	diff = image_ref + 2 * image_test

	#false positive in blue.
	mask_b = ( (diff > 1) & (diff <= 2))
	
	res_b[mask_b] = 63 #red
	res_bgr[:,:,2] = res_bgr[:,:,2] + res_b
	res_b[mask_b] = 72#green
	res_bgr[:,:,1] = res_bgr[:,:,1] + res_b
	res_b[mask_b] = 204# blue
	res_bgr[:,:,0] = res_bgr[:,:,0] + res_b
	res_b[mask_b]=90 #transparency
	res_bgr[:,:,3] = res_bgr[:,:,3] + res_b
	
		#false negative in red (a less flashy red)
	mask_r = ( (diff > 0) & (diff <= 1))
	
	res_r[mask_r] = 237 #red
	res_bgr[:,:,2] = res_bgr[:,:,2]+ res_r
	res_r[mask_r] = 28 #green
	res_bgr[:,:,1] = res_bgr[:,:,1] + res_r
	res_r[mask_r] = 36 # blue
	res_bgr[:,:,0] = res_bgr[:,:,0] + res_r
	res_r[mask_r]=35 #transparency
	res_bgr[:,:,3] = res_bgr[:,:,3] + res_r
	
	
	return res_bgr.astype('uint8')
	
	