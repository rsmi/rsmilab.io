#target_to_dataset
from rsmi import utils


class training_datasets_from_a_single_image:
	"""A class that creates a dataset as expected by the sklearn classes, from a set composed of a single image.
	For development purpose.
		
	Parameters
	----------
	image_to_classify_object: object of class "image_to_classify"
		instance of a original image loaded
	target_image_object: object of class "target_image"
		instance of a target (ground truth) image
	description_string: str
		string that describes the dataset
	
	Attributes
	----------
	dataset.DESCR: str
		description string
	dataset.data: numpy matrix
		features values of the final 1D-shaped image (row = sample, col = features)
	dataset.feature_names: list of str
		features names
	dataset.target: numpy array of int
		class(ification) ID(0 1) of the target data
	dataset.target_names: list of str
		name of the target classes
	"""


	def __init__(self, image_to_classify_object, target_image_object,description_string):
		""" Reshaping during initialization
		
		Parameters
		----------
		image_to_classify_object: object of class "image_to_classify"
			instance of a original image loaded
		target_image_object: object of class "target_image"
			instance of a target (ground truth) image
		description_string: str
			string that describes the dataset
		
		Returns
		-------
		None
		
		"""
				
		self.DESCR = description_string
		
		self.data = utils.reshape_filtered_images(image_to_classify_object) # Reshape 3D matrix (2D images with n features) into 2D matrix (1D array with n features)
		
		self.feature_names = image_to_classify_object.features_name [:]# Assign feature_names (and avoid mutable behavior of list by using [:])
		
		self.target = target_image_object.target_image.reshape(-1) # Reshape 2D matrix into 1D array, append row by row
		
		self.target_names = target_image_object.target_names[:]# Assign target_names (and avoid mutable behavior of list by using [:])
