""" Image sampler module of admiral

"""

__version__ = '0.1'

__all__ = ['image_to_classify','target_image',
			'image_subsampler','bunch_sample_constructor',
			'training_datasets_from_a_single_image']

__author__ = 'François Postic <f.postic@arvalis.fr>'

from .image_input import image_to_classify
from .target_loader import target_image
from .image_subsampling import image_subsampler
from .image_subsampling import bunch_sample_constructor
from .target_to_dataset import training_datasets_from_a_single_image