#target_loader
from rsmi import utils
import cv2
import numpy

class target_image:
	"""A class for ground truth images importation. 
	
	Parameters
	----------
	filepath: str
		path of the target (ground truth) image
	classes_names: list of str
		names of the classes found after classification (["root","soil"])
	
	Attributes
	----------
	target_image.target_image: numpy matrix
		the target (ground truth) image loaded
	target_image.target_names: list of str
		names of the target classes
	"""
	
	def __init__(self, filepath, classes_names):
		"""Initialization with import of near-binarized images (pixels value are 0 or 255),
		followed by a true binarization (*Otsu binarization leading to values equals to 0 or 1).
		Must be initialized with complete file path and the names of the target classes present in the image.
		Set the attributes 'target_image' and 'target_names'.
		
				
		Parameters
		----------
		filepath: str
			path of the target (ground truth) image
		classes_names: list of str
			names of the classes found after classification (["root","soil"])
		
		Returns
		-------
		None
		

		"""
		
		self.target_image = self.load_and_binarize(utils.extract_image_from_zip(filepath).image_inside) # The image binarized
	
		self.target_names = classes_names[:]# A list of string with target classes names ["Particles","Background"]
		
	def load_and_binarize(self, image):
		"""A Method for loading and binarizing images.

		Parameters
		----------
		image: numpy matrix (uint8)
			matrix representation of an image
		
		Returns
		-------
		numpy matrix (uint8)
			binarized matrix (values equal to 0 or 1)

		"""

		im_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # Convert to grayscale
	
		(thresh, im_bw) = cv2.threshold(im_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU) # Tresholding with Otsu method
		
		return (im_bw/255).astype('uint8')

