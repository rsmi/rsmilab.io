#image_subsampling
from rsmi import utils
from .target_loader import target_image

import numpy
import math

class image_subsampler:
	"""A class to build a dataset by subsampling a image database, with the format expected by the sklearn classes.
	The subsample is build at initialization.
	
	
	Parameters
	----------
	image_to_classify_object: object of class "image_to_classify"
		instance of a original image loaded
	target_image_object: object of class "target_image"
		instance of a target (ground truth) image
	n_samples: int
		number of pixel sampled
	
	Methods
	-------
	check_count_white_pixel:
		check if a sufficient count of white pixels, and eventually resize it
	image_subsampling_white:
		sample n pixels from white pixels (annotated as roots)
	image_subsampling_black:
		sample n pixels from black pixels (annotated as soil)

	
	Attributes
	----------
	image_subsampler.data: numpy matrix
		features values of the samples stored into a matrix with rows = samples and cols = features
	image_subsampler.target: numpy array of int
		Classes ID (0 1 / soil root) of the target for each sample
	image_subsampler.target_names: list of str
		names of the target classes
	image_subsampler.feature_names: list of str
		names of the filters applied (i.e. available "features")
	image_subsampler.nSamples: int
		number of samples extracted from image
	"""

	def __init__(self,image_to_classify_object, target_image_object, n_samples):
		"""At initialization, sample an original image (image_to_classify_object) according to the class  annotated in 'target_image_object'.
		Then set attributes data, target, target_names, features_name and nSample, used to build a training subsample for a sklearn classifier.
		
		Parameters
		----------
		image_to_classify_object: object of class "image_to_classify"
			instance of a original image loaded
		target_image_object: object of class "target_image"
			An instance of a target (ground truth) image
		n_samples: int
			number of pixel sampled
		
		Returns
		-------
		None
		
		"""
		
		
		n_samples = self.check_count_white_pixel(target_image_object,n_samples)

		if n_samples > 0 :
		
			
			selected_root_pixels = self.image_subsampling_white(target_image_object,n_samples) #Create a random sample of root element positions on image (matrix nSample x 2, with x and y positions)
			
			selected_soil_pixels = self.image_subsampling_black(target_image_object,n_samples) # Create a random sample of soil element positions on image (matrix nSample x 2, with x and y positions)
			
			sub_sample_matrix = numpy.concatenate((selected_root_pixels, selected_soil_pixels),axis = 0) # Merge the both sampling matrix vertically (row appending)

			self.data = utils.reshape_filtered_images(image_to_classify_object, sub_sample_matrix) # Get feature values of selected pixels
			
			self.target = numpy.concatenate((numpy.ones(n_samples,dtype='uint8'), numpy.zeros(n_samples,dtype='uint8')),axis = 0) #Create a traget column with n_samples ones (roots) followed by n_samples zeros (soil)

			self.target_names = target_image_object.target_names[:] # Assign target_names (and avoid mutable behavior of list by using [:])
			
			self.feature_names = image_to_classify_object.features_name[:] # Assign feature_names (and avoid mutable behavior of list by using [:])
			
			self.nSamples = n_samples
			
		else :
			
			self.nSamples = 0
			
	
	def check_count_white_pixel(self,target_image_object,n_samples):
		"""A method for checking a sufficient count of white pixels, an d eventually resize it
				
		Parameters
		----------
		target_image_object: object of class "target_image"
			instance of a target (ground truth) image
		n_samples: int
			number of pixel sampled
		
		Returns
		-------
		n_samples: int
			actual number of pixel sampled, minimum between called and available 		
		"""
		
		white_pixels = numpy.argwhere(target_image_object.target_image > 0)# get list of white pixels, returns a matrix nFoundEls x 2 with x,y coordinates
	
		n_samples = min(len(white_pixels),n_samples) # restrict to the size of available white pixels
		
		return n_samples
	
	def image_subsampling_white(self,target_image_object,n_samples):
		"""A method for sampling n pixels from annotated (in white) roots.
		Returns coordinates of white pixels
		
		Parameters
		----------
		target_image_object: object of class "target_image"
			instance of a target (ground truth) image
		n_samples: int
			number of pixel sampled
		
		Returns
		------- 
		numpy matrix of shape nSample x 2
			coordinates of the white pixels
		
		"""
		
		white_pixels = numpy.argwhere(target_image_object.target_image > 0)# get list of white pixels, returns a matrix nFoundEls x 2 with x,y coordinates
	
		random_sample = numpy.random.randint(0,high=len(white_pixels),size = n_samples)# draw a sample of n random number from a "discrete" uniform disribution between 0 and 'high' (excluded)

		return white_pixels[random_sample,:]

	def image_subsampling_black(self,target_image_object,n_samples):
		"""A method for sampling n pixels from annotated (in black) soil.
		Returns coordinates of black pixels
		
		Parameters
		----------
		target_image_object: object of class "target_image"
			instance of a target (ground truth) image
		n_samples: int
			number of pixel sampled
		
		Returns
		-------
		numpy matrix of shape nSample x 2
			coordinates of the black pixels
		"""
		
		black_pixels = numpy.argwhere(target_image_object.target_image < 1)# get list of white pixels, returns a matrix nFoundEls x 2 with x,y coordinates

		random_sample = numpy.random.randint(0,high=len(black_pixels),size = n_samples)# draw a sample of n random number from a "discrete" uniform disribution between 0 and 'high' (excluded)
	
		return black_pixels[random_sample,:]



class bunch_sample_constructor:
	""" A class  that creates a large dataset from a list of images. Extract a random number of samples in each image.
	Perform subsampling by calling instances of image_subsampler.
	
	Parameters
	----------
	original_file_list: list of str
		list of the original images filepaths
	target_file_list: list of str
		list of the target images filepaths
	image_to_classify_object: object of class "image_to_classify"
		instance of a original image loaded
	filters_module:
		module containing the image filtering method applied on images
	classes_name_list: list of str
		names of the target classes
	nSample: int
		number of targeted pixels sampled for both classes
	Sample_ratio: float
		average relative amount of pixel sampled in image. 0.1 means each image contributes for 10% of nSample
	
	Attributes
	----------
	bunch_sample_constructor.data: numpy matrix
		features values of the samples stored into a matrix with rows = samples and cols = features
	bunch_sample_constructor.features_names: list of str
		names of the filters applied (i.e. "features" available)
	bunch_sample_constructor.target: numpy array of int
		Classes ID (0 1 2...) of the taregt for each sample
	bunch_sample_constructor.target_names: list of str
		names of the target classes
	
	"""
	
	
	def __init__(self, original_file_list, target_file_list, image_to_classify_object, filters_module, classes_name_list, nSample, Sample_ratio):
	

		file_index  = numpy.random.randint(0,len(original_file_list), size = 10*len(original_file_list)) #initialize a large set of image used
	
		mu = nSample*Sample_ratio #  mean of number of pixels sampled in a image
		
		max_value_local_randint = (mu*2) # max value (excluded) of a random discrete uniform distibution of mean mu
		
		i = 0 #index of image list
		
		image_count = 0 # actual count of sampled images
		
		count = 0 #count of sampled pixels 
		
		while count < nSample :
	
			local_nSample = numpy.random.randint(1, max_value_local_randint, size= 1) # set the number of sample for this image
			
			local_nSample = min(local_nSample, nSample-count) # ensure getting exactly nSample # Note that nSample-count !=0
			
			print('')
			print('----------------------------------------')
			print("Seeking for :",format(math.ceil(local_nSample/2))+' samples' )
			
			
			filepath  = original_file_list[file_index[i]] # get the original image
			
			filepath_target = target_file_list[file_index[i]] # get the target image
			
			target_image_object = target_image(filepath_target, classes_name_list) # create object target image

			image_to_classify_object.read_and_filter(filepath,filters_module, def_att_dict = image_to_classify_object.Attributes_dict)  # apply filters to a image located at input_images_path

			image_data_set = image_subsampler(image_to_classify_object, target_image_object,math.ceil(local_nSample/2)) # get a dataset of (maximum)local_nSample sampled inside this image

			local_nSample = image_data_set.nSamples
			
			print("Extracted :",format(local_nSample)+' samples')	
			
			if image_data_set.nSamples >0 : # in case of existing samples

				if image_count <1 :
				
					feature_names = image_to_classify_object.features_name[:] # initialize the features names on the first image (all images should have the same "features_names")
					
					target_names = target_image_object.target_names[:] # same as above (also avoid mutable behavior of list)
				
					data = image_data_set.data # initialize with first subsample dataset
					
					target = image_data_set.target# initialize with first subsample dataset
					
					
				else:
										
					data = numpy.concatenate((data,image_data_set.data),axis = 0) # concatenate vertically (adding rows) the training vector (features of each sample)
					
					target = numpy.concatenate((target,image_data_set.target),axis = 0)# concatenate vertically (adding rows) the target values
			
				count += int(local_nSample) # accumulate the count of samples
			
				image_count += 1 # increment the count of sampled images
			
			
			print("Remaining :",format(int(nSample-count))+' samples')
			print('----------------------------------------')
			
			i += 1  # next image index
			
			
			
		self.count = count
		
		print("final count",format(count))
		
		self.data = data
		print(data.shape)
		self.feature_names = feature_names
		
		self.target = target
		
		self.target_names = target_names
			
			