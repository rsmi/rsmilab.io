#image_input
from rsmi import utils
import cv2


class image_to_classify():
	"""A class that imports an image and applies filters imported from a module.
	Original and filtered images are store as attributes of this class.
	The default values of each filter (e.g. kernel default size) are set in a dictionary (Attributes_dict), as tuples.
	
	Parameters
	----------
	filters_module: module containing the image filtering method applied on images
	method_list: str list
		list of the filters called from the module
	
	Methods
	-------
	set_method_dict_default_value: 
		Set the dictionary for the default values of each. Used to set kernel_sizes in read_and_filter method.
	read_and_filter:
		read an image and apply available filtering methods in the object

	
	Attributes
	----------
	image_to_classify.image: numpy matrix
		original image loaded (uint8)
	image_to_classify.featureN: numpy matrix
		filtered image used as Nth feature for classification
	image_to_classify.features_name: list of str
		names of the filters applied (i.e. "features" available)
	image_to_classify.Attributes_dict: dict
	dictionary of required values intended for kernel_sizes
	
	"""
	
	def __init__(self,filters_module, method_list):

		setattr(self, 'features_name', method_list[:]) # Creates an attribute listing (order matters) filter-based features
		
		self.Attributes_dict = {} #initialize a dictionnary that store the expected input value for each method
		

	def set_method_dict_default_value(self,filters_module,ksize=None):
		"""Set the dictionary  'image_to_classify.Attributes_dict' to the default values.
		Used to set kernel_sizes in read_and_filter method. default values are stored as tuples.
		
		Parameters
		----------
		filters_module:
			module containing the image filtering method applied on images
		ksize: int (optional, default = 3)
			value for ksize
		
		Returns
		-------
		None
		"""
		
		if ksize is None: #
			
			ksize = 3 # default ksize
		
		
		for method_name in self.features_name :
			
			filters_method = getattr(filters_module, method_name) # get the corresponding filtering method located in the filters_module
			
			n_args = filters_method.__code__.co_argcount # number of args required by this function (self included)
	
			if n_args>1 : #case where the method require a kernel size (ksize)
			
				self.Attributes_dict[str(method_name)] = (ksize)
				
			else: # case where no supplementary attributes are required
			
				self.Attributes_dict[str(method_name)] = None
				

	def read_and_filter(self,filepath,filters_module,ksize=None,def_att_dict=None):
		""" A method that reads an image and applies available filtering methods in the object.
		Set the image_to_classify.image (original image loaded - uint8) and image_to_classify.featureN attributes (filtered image used as Nth feature for classification)
		
		Parameters
		----------
		filepath: str
			path of the original image to classify
		filters_module: 
			module containing the filters to apply on images
		ksize: int (optional, default = 3)
			value for ksize for all kernel based filter(optional)
		def_att_dict: dict (optional, default = None)
			dictionary with default attributes values
		
		Returns
		-------
		None
		"""
		
		self.image = cv2.imread(filepath) # Import an BRG image (as usual with openCV)
		
		if def_att_dict is None :
		
			self.set_method_dict_default_value(filters_module,ksize) # Set the default ksize for all methods that require this argument
		
		
		for i in self.features_name :

			arg_tuple = self.Attributes_dict[str(i)] # Extract the tuple of default value ( ex : None or (ksize) )
			
			new_attribute_name = 'feature_'+str(i) # create the name of the attribute that stores the filtered image
				
			applied_method = getattr(filters_module, i) # get the method to apply
			
			if arg_tuple is None: # Case where no supplementary arg is required
				
				setattr( self,new_attribute_name,applied_method(self.image) ) # apply the filter method to the new attribute
				
			else:# Case where supplementary args are required
				
				setattr( self,new_attribute_name,applied_method(self.image, arg_tuple) )# apply the filter method to the new attribute
				
				