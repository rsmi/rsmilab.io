.. image:: Logo1alt.png  
   :scale: 15 %
   :align: right


Create a database
-----------------

This example shows how to create a subsample from a set of several images. In our case image are contained in zip files (tiff files in zip archives as given by ImageJ “save as ZIP”)
The sampleRatio is the average relative number of samples drawn from a single image.
The dataset is pickled in an object which attributes X y and Descr according to the sklearn inputs

.. code-block:: python

	#create database example

	from rsmi import image_sampler
	from rsmi import filters

	import joblib
	import os

	## set directories names
	#

	original_dir_name = 'original/'

	groundtruth_dir_name = 'groundtruth/'

	dataset_generated = 'database_ex1.pkl'

	##Set the sampling characteristics
	#
	n_samples = 1e4 # ask for 10 000  pixels of each class

	Sample_ratio = 0.10 # each image gives 10% of the dataset

	classes_names = ['root','soil']

	##Set the image filtering methods used here (found in the 'filters' module)
	#
	method_list  = ['Grayscale',
					'R_channel','G_channel','B_channel',
					'H_channel','S_channel','V_channel',
					'Rnorm_channel','Gnorm_channel','Bnorm_channel']
					

	##end of user input

	##Set images used for pixel sampling
	##
	script_dir = os.path.dirname(__file__) #absolute dir of this script

	original_dir = os.path.join(script_dir, original_dir_name)

	groundtruth_dir = os.path.join(script_dir, groundtruth_dir_name)

	original_path = [os.path.join(original_dir, f) for f in os.listdir(original_dir)]

	groundtruth_path = [os.path.join(groundtruth_dir, f) for f in os.listdir(groundtruth_dir)]


	##Set path of the dataset generated
	#
	dataset_generated_filepath = os.path.abspath(os.path.join(script_dir,dataset_generated))


	##Create an instance of the image_to_classify object, with 'filters' module and 'method_list' list of image processing filters applied
	#				
	image_to_classify_object = image_sampler.image_to_classify(filters,method_list) # initialize an image_to_classify_object
	image_to_classify_object.set_method_dict_default_value(filters)
		
	## Call the sampling method via instanciation of a image_sampler.bunch_sample_constructor object
	#
	subsamplerobj = image_sampler.bunch_sample_constructor(original_path, groundtruth_path, image_to_classify_object, 
															   filters, classes_names, n_samples, Sample_ratio)				
															   
	##Save the dataset into a pickle
	#
	joblib.dump(subsamplerobj, dataset_generated_filepath) # save datasets	   



Train a classifier
------------------

Here, we train a classifier from the pickled database

.. code-block:: python

	#Train a classifier example

	from sklearn.ensemble import RandomForestClassifier

	import joblib
	import os


	##Set filename of pickled database, and of the future classifier pickled
	#
	dataset_loaded = 'database_ex1.pkl'

	classifier_pickled = 'trained_clf_ex2.pkl'

	##Create a random forest classifier from sklearn
	#
	randomforest_classifier = RandomForestClassifier()

	##end of user input



	script_dir = os.path.dirname(__file__) #absolute dir of this script

	dataset_loaded_filepath = os.path.abspath(os.path.join(script_dir,dataset_loaded))

	clf_pickled_filepath = os.path.abspath(os.path.join(script_dir,classifier_pickled))


	subsamplerobj = joblib.load(dataset_loaded_filepath) # load dataset

	X = subsamplerobj.data

	y = subsamplerobj.target

	randomforest_classifier.fit(X,y)

	joblib.dump(randomforest_classifier,clf_pickled_filepath)


Set ranking
-----------

Ranking the best features set is performed via a Randomized Logistic regression. The outputted scores correspond to the rate of selection of a feature as relevant. Higher scores mean higher importance (1.00 means chosen in 100% of the cases).

.. code-block:: python

	#rank features

	from rsmi import utils
	from rsmi import feature_ranking

	import os
	import joblib


	##Set filename of pickled database
	#
	dataset_loaded = 'database_ex1.pkl'

	csv_file = 'set_ranks_ex3.csv'

	n_folds = 3 # number of cross-validation folds

	script_dir = os.path.dirname(__file__) #absolute dir of this script

	dataset_loaded_filepath = os.path.abspath(os.path.join(script_dir,dataset_loaded))

	csv_rank_file_path = os.path.abspath(os.path.join(script_dir,csv_file))



	subsamplerobj = joblib.load(dataset_loaded_filepath) # load dataset


	print('Initiate feature ranking')


	rank_dictionnary = feature_ranking.set_ranking(subsamplerobj,  n_folds, score = True)#univariate ranking 

	utils.dict_to_csv(csv_rank_file_path, rank_dictionnary)#save to csv

	print('Example 3: Stability ranking done')


Classify an image
-----------------

In this step, we apply a trained classifier to an image. An instance of class Imagetoclassify is generated, that will handle the image and compute its features images (after image filtering)

.. code-block:: python


Classifier ranking
------------------

A simple step consist in testing several classifiers, and rank them according to a score, like the precision or ROC AUC. Here, the F0.5 is computed for the optimal threshold (actually computed at each threshold value). 


.. code-block:: python

	#Classifier ranking

	#-*- coding: utf-8 -*-
	from rsmi import feature_ranking
	from rsmi import utils

	#Let's import some classifiers from the scikit learn
	from sklearn.svm import LinearSVC
	from sklearn.naive_bayes import GaussianNB
	from sklearn.ensemble import RandomForestClassifier


	from sklearn.model_selection import train_test_split

	import joblib
	import os
	import numpy


	##Instantiation of different classifiers with their default options
	#
	Naive_bayes_classifier = GaussianNB()#Naive Bayes (classifier on continuous features)

	LinSVM_classifier = LinearSVC()#Linear SVM

	randomforest_classifier = RandomForestClassifier()#Random forest

	##Set the list of the classifiers used
	#
	classif_list = [ Naive_bayes_classifier, LinSVM_classifier, randomforest_classifier ]#First, the list of object instances

	classif_names = ['Naive Bayes', 'Linear SVM','Random Forest']#Then, their name in the dictionary

	##Set filename of pickled database
	#
	dataset_loaded = 'database_ex1.pkl'

	##Set the ranking parameters
	#
	nfolds = 3 # number of folds for cross-validation

	features_ranks = [1,2,3,4,5,6,7,8,9,10] #

	nSample = 50000#

	nbins = 20 #

	##Set filename of the saved F0.5 score table
	#
	score_table = 'clf_ranks_ex5.csv'


	script_dir = os.path.dirname(__file__) #absolute dir of this script

	csv_rank_file_path = os.path.abspath(os.path.join(script_dir,score_table))



	##Load the dataset
	#
	script_dir = os.path.dirname(__file__) #absolute dir of this script

	dataset_loaded_filepath = os.path.abspath(os.path.join(script_dir,dataset_loaded))


	X = subsamplerobj.data

	y = subsamplerobj.target
		
	n_observation = len(y)#get the size of the whole dataset



	## Draw a random subset of the dataset
	#
	split_size = nSample/n_observation#Set the percentage of the data sampled

	X_dump, X_sampled, y_dump, y_sampled  = train_test_split(X, y, test_size=split_size, random_state=None, stratify = y)


	##Compute the F0.5 score for a varying number of feature used
	#
	Classif_rank_dictionnary_F05 = feature_ranking.classif_F05_score(classif_list, classif_names, X_sampled, y_sampled, nfolds, features_ranks, nbins = nbins  ) 

	utils.dict_to_csv(save_path , Classif_rank_dictionnary_F05)#save to csv

