.. image:: Logo1alt.png  
   :scale: 15 %
   :align: right

:mod:`rsmi`.feature_ranking -- Feature scoring module
=====================================================

.. module:: feature_ranking
   :platform: Unix, Windows
   :synopsis: Provides method for scoring and ranking generated features.
.. codeauthor:: Francois Postic <f.postic@yahoo.fr>
.. moduleauthor:: Francois Postic <f.postic@yahoo.fr>

Description
-----------

Module containing the different classification scores used during the features selection stage.

Module contents
---------------


.. automodule:: rsmi.feature_ranking.univariate_ranking
    :members:

.. automodule:: rsmi.feature_ranking.set_ranking
    :members:

.. automodule:: rsmi.feature_ranking.classif_roc_auc_score
    :members:

.. automodule:: rsmi.feature_ranking.classif_F05_score
    :members:

.. automodule:: rsmi.feature_ranking.classif_precision_score
    :members:
