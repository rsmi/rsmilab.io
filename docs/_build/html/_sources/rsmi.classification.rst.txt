.. image:: Logo1alt.png  
   :scale: 15 %
   :align: right

:mod:`rsmi`.classification -- Image classification module
=========================================================

.. module:: classification
   :platform: Unix, Windows
   :synopsis: Applies trained classifiers on image files.
.. codeauthor:: Francois Postic <f.postic@yahoo.fr>
.. moduleauthor:: Francois Postic <f.postic@yahoo.fr>

Description
-----------

Main class for root detection from a trained classifier.
Generates prediction images for the root class.

Module contents
---------------

.. autoclass:: rsmi.classification.image_classifier
    :members:

