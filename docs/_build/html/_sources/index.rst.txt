.. rsmi documentation master file, created by
   sphinx-quickstart on Tue Dec 12 17:48:10 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: Logo1alt.png  
   :scale: 50 %
   :align: center
   
Welcome to RSMI's documentation
=======================================
(images resultats)

This Python package provides tools for building a pixel-based classification of roots on minirhizotron images. This package provides methods and classes implemented in different modules that can be used as a base for a complete classification algorithm, from image sampling and feature ranking to classification and comparison with ground truth images. This package is intended to be used conjointly with the scikit-learn (sklearn) and the OpenCV (cv2) packages, for machine learning and image processing tasks, respectively.
This package was developed on Linux with Python 3.6, sklearn 0.19.1 and OpenCV 3.0.0, and tested on Windows.
This documentation provides some examples for image manipulation, classifier training, feature ranking and image classification to facilitate test and adaptation to other kind of images.
As illustrated below, an original RGB image (1) is transformed using several image processing filters (2), each filter adds a feature to the image data (3), which in turn creates a new dimension that leads to a feature vector for each pixel. Each feature vector is then classified and the whole image is reconstructed (4), and potentially compared with the ground truth (5).


.. image:: Image1.png


Setup
-----
This module is implemented in Python 3.4, and requires Scikit-learn 0.19.1 and OpenCV 3.0.0

**For Ubuntu 14.04 LTS and newer:**

.. code-block:: bash

    $ sudo apt-get install python3
    $ sudo apt-get install python3-pip
    $ sudo pip install sklearn opencv-python==2.4.9


Then clone this git:

.. code-block:: bash

    $ git clone https://gitlab.com/rsmi/rsmi.gitlab.io


You may want to perfom unit test:

.. code-block:: bash

 $ cd rsmi.gitlab.io
 $ python3 test_main.py
 


Examples
--------
.. toctree::
   :maxdepth: 1

   examples



API
---
.. toctree::
   :maxdepth: 2

   rsmi




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
