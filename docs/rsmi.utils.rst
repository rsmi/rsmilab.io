.. image:: Logo1alt.png  
   :scale: 15 %
   :align: right

:mod:`rsmi`.utils -- Utility module
===================================

.. module:: utils
   :platform: Unix, Windows
   :synopsis: Provides numerous function for handling files.
.. codeauthor:: Francois Postic <f.postic@yahoo.fr>
.. moduleauthor:: Francois Postic <f.postic@yahoo.fr>


Description
-----------

Utilitary methods of RSMI.


Subpackages
-----------

.. toctree::

    rsmi.utils.kernel_generator


Module contents
---------------
.. automodule:: rsmi.utils
    :members:

