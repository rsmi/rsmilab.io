.. image:: Logo1alt.png  
   :scale: 15 %
   :align: right

:mod:`rsmi`.filters -- Image processing module
==============================================

.. module:: filters
   :platform: Unix, Windows
   :synopsis: Collection of common image filtering procedure from OpenCv.
.. codeauthor:: Francois Postic <f.postic@yahoo.fr>
.. moduleauthor:: Francois Postic <f.postic@yahoo.fr>

Description
-----------

Image transformation used in RSMI for the features generation.
Methods implemented here do not alter image dimensions.


Module contents
---------------

.. automodule:: rsmi.filters
    :members:

