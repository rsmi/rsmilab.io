.. image:: Logo1alt.png  
   :scale: 15 %
   :align: right

Modules
-------
.. toctree::

    rsmi.classification
    rsmi.feature_ranking
    rsmi.filters
    rsmi.image_sampler
    rsmi.utils

