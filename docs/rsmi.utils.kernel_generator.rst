.. image:: Logo1alt.png  
   :scale: 15 %
   :align: right

:mod:`rsmi`.utils.kernel_generator -- Feature scoring module
============================================================

.. module:: utils.kernel_generator
   :platform: Unix, Windows
   :synopsis: Custom sized kernels for Sobel Prewitt and laplacian filters.
.. codeauthor:: Francois Postic <f.postic@yahoo.fr>
.. moduleauthor:: Francois Postic <f.postic@yahoo.fr>

Description
-----------

Kernel genreator module for spatial filtering.

Module contents
---------------

.. automodule:: rsmi.utils.kernel_generator.laplacian_kernel_generator
    :members:


.. automodule:: rsmi.utils.kernel_generator.prewitt_kernel_generator
    :members:

.. automodule:: rsmi.utils.kernel_generator.sobel_kernel_generator
    :members:

