.. image:: Logo1alt.png  
   :scale: 15 %
   :align: right

:mod:`rsmi`.image_sampler -- Sampling procedure module
======================================================

.. module:: image_sampler
   :platform: Unix, Windows
   :synopsis: Provides method for sampling a image data base and for generating dataset.
.. codeauthor:: Francois Postic <f.postic@yahoo.fr>
.. moduleauthor:: Francois Postic <f.postic@yahoo.fr>


Description
-----------

Tools for building training datasets.


Module contents
---------------

.. autoclass:: rsmi.image_sampler.image_to_classify
    :members:

.. autoclass:: rsmi.image_sampler.target_image
    :members:

.. autoclass:: rsmi.image_sampler.image_subsampler
    :members:

.. autoclass:: rsmi.image_sampler.bunch_sample_constructor
    :members:    

.. autoclass:: rsmi.image_sampler.training_datasets_from_a_single_image
    :members:
