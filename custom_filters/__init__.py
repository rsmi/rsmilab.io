# custom_filter_module
__all__ = ['Grayscale',
			'R_channel','G_channel','B_channel',
			'H_channel','S_channel','V_channel',
			'Rnorm_channel','Gnorm_channel','Bnorm_channel',
			'Hnorm_channel','Snorm_channel','Vnorm_channel',
			'gray_minimum','gray_maximum','gray_mean','gray_variance','gray_sd',
			'gray_sobel', 'gray_prewitt','gray_laplacian',
			'gray_norm_sobel', 'gray_norm_prewitt','gray_norm_Laplacian']


from rsmi import utils
from rsmi import filters
from rsmi.filters import * #import all non modified filters
import numpy
import cv2

def gray_minimum(image,ksize):

	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	return filters.minimum(gray_image,ksize)
	

def gray_maximum(image,ksize):

	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	return filters.maximum(gray_image,ksize)


def gray_mean(image,ksize):

	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	return filters.mean(gray_image,ksize)
	

def gray_variance(image,ksize):

	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	return filters.variance(gray_image,ksize)
	

def gray_sd(image,ksize):

	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	return filters.sd(gray_image,ksize)

	
def gray_sobel(image,ksize):

	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	return filters.sobel(gray_image,ksize)
	
	
def gray_prewitt(image,ksize):

	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	return filters.prewitt(gray_image,ksize)

	
def gray_laplacian(image,ksize):

	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	return filters.laplacian(gray_image,ksize)
	
	
def gray_norm_sobel(image,ksize):
	"""Method that computes directionnal sobel in x direction and y direction on a kernel of ksize (must be odd),
	on the grayscale colorspace with a normaliazation factor.
	Arguments:
	- image : a numpy matrix of the image
	- ksize: width (or height) of a square kernel used in computation

	Output:
	- a uint8 numpy matrix
	"""
	
	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	ddepth = cv2.CV_64F # destination bit depth

	kernel_Gx = utils.kernel_generator.sobel_x(ksize) # kernel for a horizontal sobel
	
	kernel_Gy = utils.kernel_generator.sobel_y(ksize) # kernel for a vertical sobel
	
	weight = 2/numpy.sum(abs(kernel_Gx))

	sobelx_64f = cv2.filter2D(gray_image, ddepth , weight * kernel_Gx) # horizontal sobel
	
	sobely_64f = cv2.filter2D(gray_image, ddepth , weight *kernel_Gy) # vertical sobel

	sobel_64f = numpy.sqrt(numpy.square(sobelx_64f)+ numpy.square(sobely_64f)) # full sobel computed as sqrt(x²+y²)

	return cv2.convertScaleAbs(sobel_64f) # return a 8 bit unsigned numpy matrix
	

def gray_norm_prewitt(image,ksize):
	"""Method that computes directionnal prewitt in x direction and y direction on a kernel of ksize (must be odd),
	on the grayscale colorspace with a normaliazation factor.
	Arguments:
	- image : a numpy matrix of the image
	- ksize: width (or height) of a square kernel used in computation

	Output:
	- a uint8 numpy matrix
	"""
	
	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
	
	ddepth = cv2.CV_64F # destination bit depth

	kernel_Gx = utils.kernel_generator.prewitt_x(ksize) # kernel for a horizontal prewitt
	
	kernel_Gy = utils.kernel_generator.prewitt_y(ksize) # kernel for a vertical prewitt
	
	weight = 2/numpy.sum(abs(kernel_Gx))
	
	prewittx_64f = cv2.filter2D(gray_image, ddepth , weight * kernel_Gx) # horizontal prewitt
	
	prewitty_64f = cv2.filter2D(gray_image, ddepth , weight * kernel_Gy) # vertical prewitt

	prewitt_64f = numpy.sqrt(numpy.square(prewittx_64f)+ numpy.square(prewitty_64f)) # full prewitt computed as sqrt(x²+y²)

	return cv2.convertScaleAbs(prewitt_64f) # return a 8 bit unsigned numpy matrix
	

def gray_norm_Laplacian(image,ksize):
	"""Method that computes directionnal laplacian in x direction and y direction on a kernel of ksize (must be odd),
	on the grayscale colorspace with a normaliazation factor.
	Arguments:
	- image : a numpy matrix of the image
	- ksize: width (or height) of a square kernel used in computation

	Output:
	- a uint8 numpy matrix
	"""
	
	gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale
		
	ddepth = cv2.CV_64F # destination bit depth
	
	kernel = utils.kernel_generator.laplacian(ksize)
	
	weight = 2/numpy.sum(abs(kernel))

	laplacian_64f = cv2.filter2D(gray_image, ddepth , weight * kernel) # horizontal prewitt

	return cv2.convertScaleAbs(laplacian_64f) # return a 8 bit unsigned numpy matrix
