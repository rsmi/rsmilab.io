#test
import test
import traceback
import time

print('------------------------------------------ ')
print('         rsmi Test module')
print('------------------------------------------')
print('')

time.sleep(0.5) 
print('--------------------------- ')
print('Test 1 :filters behavior')
print('--------------------------- ')
print('')

try:
	test.filter_test()
	
	print('---------')
	print('Test 1 completed')
	
except Exception as e:
	print('---------')
	print("Test 1 failed")
	print (e)
	print('')
	print('Traceback info :')
	print('----------------')
	traceback.print_exc()

print('')
print('--------------------------- ')



time.sleep(1) 
print('--------------------------- ')
print('Test 2 : load a orignal image ')
print('--------------------------- ')
print('')
try:
	test.input_test()
	
	print('---------')
	print('Test 2 completed')


except Exception as e:
	print('---------')
	print("Test 2 failed")
	print (e)
	print('')
	print('Traceback info :')
	print('----------------')
	traceback.print_exc()
	
print('')
print('--------------------------- ')


time.sleep(1) 

print('--------------------------- ')
print('Test 3 : subampling an image')
print('--------------------------- ')
print('')
try:
	test.subsampling_test()
	
	print('---------')
	print('Test 3 completed')


except Exception as e:
	print('---------')
	print("Test 3 failed")
	print (e)
	print('')
	print('Traceback info :')
	print('----------------')
	traceback.print_exc()
	
print('')
print('--------------------------- ')



time.sleep(1) 

print('---------------------------------- ')
print('Test 4 : subampling a set of images')
print('---------------------------------- ')
print('')
try:
	test.multiple_sampling_test()
	
	print('---------')
	print('Test 4 completed')


except Exception as e:
	print('---------')
	print("Test 4 failed")
	print (e)
	print('')
	print('Traceback info :')
	print('----------------')
	traceback.print_exc()
	
print('')
print('--------------------------- ')


print('---------------------------------- ')
print('Test 5 : train a classifier        ')
print('---------------------------------- ')
print('')
try:
	test.train_save_test()
	
	print('---------')
	print('Test 5 completed')


except Exception as e:
	print('---------')
	print("Test 5 failed")
	print (e)
	print('')
	print('Traceback info :')
	print('----------------')
	traceback.print_exc()
	
print('')
print('--------------------------- ')


print('---------------------------------- ')
print('Test 6 : load and apply a classifier        ')
print('---------------------------------- ')
print('')
try:
	test.load_classif_test()
	
	print('---------')
	print('Test 6 completed')


except Exception as e:
	print('---------')
	print("Test 6 failed")
	print (e)
	print('')
	print('Traceback info :')
	print('----------------')
	traceback.print_exc()
	
print('')
print('--------------------------- ')


print('---------------------------------- ')
print('Test 7 : Score some features       ')
print('---------------------------------- ')
print('')
try:
	test.dataset_scoring_test()
	
	print('---------')
	print('Test 7 completed')


except Exception as e:
	print('---------')
	print("Test 7 failed")
	print (e)
	print('')
	print('Traceback info :')
	print('----------------')
	traceback.print_exc()
	
print('')
print('--------------------------- ')



