#load_classif_test
from rsmi import classification
from rsmi import image_sampler
import custom_filters

import numpy
import joblib
import os
import cv2

import sklearn
from sklearn import linear_model
from sklearn import ensemble

def test():

	pickled_classifier_path = 'test/samples/classif.pkl'
	
	input_image_path = 'test/samples/grxA2_T5_L5_16.12.14_122234_2_postic_cropped.jpg'
	
	filter_module = custom_filters	
	
	filters_method= ['H_channel','gray_sd','gray_maximum']
	
	ksize = 3
	
	
	trained_classifier_object = joblib.load(pickled_classifier_path)
	
	
	image_to_classify_object =  image_sampler.image_to_classify(filter_module, filters_method )
	
	image_to_classify_object.read_and_filter(os.path.abspath(input_image_path),filter_module, ksize)
	
	classified_object = classification.image_classifier()

	classified_object.predict(image_to_classify_object, trained_classifier_object, threshold = 0.65)


	cv2.imshow('Original',image_to_classify_object.image)
	cv2.imshow('classified',classified_object.image_classified)

	cv2.waitKey(1000)
	cv2.destroyAllWindows()
