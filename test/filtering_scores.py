#filtering_scores
import numpy
import cv2


def wrap_filtering_score(filter_method,input_path,expected_path):
	"""A wrapper for using filtering_score with image paths
	
	Arguments:
	- filter_method : method ; the filtering method under test
	- input_image : str ; path of an image of an input
	- expected_image : str ; path of an image of the expected result
	
	Output:
	-rmse : float; RMSE score
	
	"""
	
	input_image = cv2.imread(input_path)

	expected_image = cv2.imread(expected_path)
	
	return filtering_score(filter_method,input_image,expected_image)


def filtering_score(filter_method,input_image,expected_image):
	"""Method for testing a filtering method. Computes the Root Mean Squared Difference (RMSE) between an input image and its expected result.
	
	Arguments:
	- filter_method : method ; the filtering method under test
	- input_image : numpy array ; a matrix of an image of an input
	- expected_image : numpy array ; a matrix of an image of the expected result
	
	Output:
	-rmse : float; RMSE score
	"""	
	n_args = filter_method.__code__.co_argcount # number of args required by this function (self included)
	
	if n_args>1 : # if more than self +1 (the latter is 'image')
		print(filter_method.__name__)
	
		ksize = 3 # set the kernel size to 3
		
		filtered_image = filter_method(input_image,ksize)
	
	else: # no other required arg
	
		filtered_image = filter_method(input_image)
		

	

	if len(filtered_image.shape) < 3 : # case of graylevel comparison

		expected_image = cv2.cvtColor(expected_image,cv2.COLOR_BGR2GRAY) # convert a RGB into grayscale 
	
	"""
	cv2.imshow('opencv',filtered_image)
	cv2.imshow('expected',expected_image)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
	"""
	
	rmse = img_rmse(expected_image,filtered_image)
	
	return rmse


def img_rmse(image1,image2):
	"""Method that computes the Root Mean Squared Difference (RMSE) between two images.
	
	Arguments:
	- image1 : numpy array ; a matrix of an image
	- image2 : numpy array ; a matrix of an image
	
	Output:
	-rmse : float; RMSE score
	"""
	
	m = float(image1.shape[0]) # height
	
	n = float(image1.shape[1]) # width
	
	residuals = image1.astype("float")-image2.astype("float") # difference between image1 and image2
	
	sum_squared_residuals = numpy.sum(numpy.power(residuals, 2)) # squared difference
	
	rmse = numpy.sqrt(sum_squared_residuals / (m * n ))
	
	return 100*rmse/256
