#subsampling_test
from rsmi import image_sampler
from rsmi import filters

from rsmi import utils
import os
import cv2


def test():
	
	input_image_path = 'test/samples/grxA2_T5_L5_16.12.14_122234_2_postic.jpg'
	
	target_image_path = 'test/samples/grxA2_T5_L5_16.12.14_122234_2_postic.zip'

	filter_module = filters

	filters_method= ['H_channel','sd','maximum']

	ksize = 3

	classes_names = ['root','soil']

	target_fullpath  = os.path.abspath(target_image_path)

	input_fullpath  = os.path.abspath(input_image_path)


	target_image_object = image_sampler.target_image(target_fullpath, classes_names)


	image_to_classify_object = image_sampler.image_to_classify(filter_module,filters_method) # initialize an image_to_classify_object

	image_to_classify_object.set_method_dict_default_value(filter_module,ksize)# initialize its default value dictionnay with ksize

	image_to_classify_object.read_and_filter(input_fullpath,filters, ksize) # apply filters to a image located at input_image_path


	print('')
	print('Subsampling test : ')
	print('-------------------')

	n_samples = 5 # ask for 10 pixels of each class (20pixels)

	print('number of expected pixels sampled : ', 2*n_samples)
	print('')

	subsamplerobj = image_sampler.image_subsampler(image_to_classify_object, target_image_object, n_samples)

	print('dataset generated :')
	print(subsamplerobj.data )
	print('')
	print('target generated :')
	print(subsamplerobj.target )
	print('')
	print('classes names generated :')
	print(subsamplerobj.target_names )
	print('')
	print('features generated :')
	print(subsamplerobj.feature_names )
	print('')
	print('samples generated :')
	print(subsamplerobj.nSamples )
