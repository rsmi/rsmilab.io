""" test module of rsmi

"""

__version__ = '0.1'

__all__ = ['filter_test','input_test','subsampling_test','multiple_sampling_test',
			'custom_feature_test','train_save_test','load_classif_test', 'dataset_scoring_test']

__author__ = 'François Postic <f.postic@arvalis.fr>'


#test
def filter_test():

	from .filters_import import score
	score()

def input_test():

	from .image_to_classify_test import test
	test()
	
def subsampling_test():
	
	from .subsampling_test import test
	test()
	
def multiple_sampling_test():

	from .several_images_sampling_test import test 
	test()
	
	
def custom_feature_test():

	from .custom_features_test import test 
	
	test()

def train_save_test():

	from .train_save_test import test 
	
	test()

	
def load_classif_test():

	from .load_classif_test import test 
	
	test()

	
def dataset_scoring_test():
	from .dataset_scoring_test import test

	test()