#custom_features_test


from rsmi import image_sampler
from rsmi import filters

from rsmi import utils
import os
import cv2

import importlib


def test():
	import rsmi
	from rsmi import filters
	
	input_image_path = 'test/samples/woman.jpg'
	
	print(dir(filters))
	
	for k in range(1,3):
	
		ksize = 2*k + 1
	
		new_feature = 'sobel_'+str(ksize) 
	
		original_function = getattr(filters,'sobel')
		
		print('ori : ',original_function)
		
		new_func = customizer(original_function, ksize)
		

		print('customized :', new_func)
		print('customized :', new_func.__defaults__)
		
		setattr(filters, new_feature, new_func)
		
	print(dir(filters))
	
	importlib.reload(rsmi)
	importlib.reload(filters)
	
	print(dir(filters))
	
	
	 
	 
	 
	 
"""
setattr(local_filters_module,new_feature)


filters_method = ['laplacian','sobel','prewitt','scharr', 'minimum', 'maximum', 'mean', 'sd' ]"""

def customizer(original_function, new_def_arg):

	def wrapper(original_function):

		return original_function
		
	wrapper.__defaults__=(new_def_arg,)
	print('wrapper:',wrapper.__defaults__)


	return wrapper
