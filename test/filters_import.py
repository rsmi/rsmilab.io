#filters_import
from rsmi import image_sampler
from rsmi import filters
from .filtering_scores import wrap_filtering_score

import os
import cv2


def score():
	"""
	"""

	input_image_path = ['test/samples/flea_market.jpg','test/samples/flea_market.jpg','test/samples/flea_market.jpg',
						'test/samples/flea_market.jpg','test/samples/flea_market.jpg','test/samples/flea_market.jpg',
						'test/samples/flea_market.jpg','test/samples/robots-rgb.jpg','test/samples/robots-rgb.jpg',
						'test/samples/robots-rgb.jpg','test/samples/woman.jpg','test/samples/woman.jpg',
						'test/samples/woman.jpg','test/samples/hip.jpg','test/samples/hip.jpg',
						'test/samples/hip.jpg','test/samples/coke.jpg']
						
	expected_image_path = ['test/samples/flea_market-gray.jpg','test/samples/flea_market-red.jpg','test/samples/flea_market-green.jpg',
							'test/samples/flea_market-blue.jpg','test/samples/flea_market-hue.jpg','test/samples/flea_market-saturation.jpg',
							'test/samples/flea_market-value.jpg','test/samples/robots-normalized-red.jpg','test/samples/robots-normalized-green.jpg',
							'test/samples/robots-normalized-blue.jpg','test/samples/woman-sobel.jpg','test/samples/woman-scharr.jpg',
							'test/samples/woman-lap3x3.jpg','test/samples/hip-minimum.jpg','test/samples/hip-maximum.jpg',
							'test/samples/hip-arithmeticmean.jpg','test/samples/coke-sd.jpg']
							
							
	filters_method = ['Grayscale', 'R_channel','G_channel','B_channel','H_channel','S_channel','V_channel',
			'Rnorm_channel','Gnorm_channel','Bnorm_channel','sobel','scharr', 'laplacian','minimum', 'maximum', 'mean', 'sd' ]
			
	filter_receiver_obj =  image_sampler.image_to_classify(filters, filters_method )

	for i, j, k in zip(input_image_path, expected_image_path,filters_method): 
	
		absolute_input_image_path = os.path.abspath(j)# absolute path are required for opencv
	
		absolute_expected_image_path = os.path.abspath(j)# absolute path are required for opencv
		
		rmse = wrap_filtering_score(getattr(filters,k),absolute_input_image_path,absolute_expected_image_path)
		
		print('rmse of ',k, ' :',rmse)
		print('')
	return

	
