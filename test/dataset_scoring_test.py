# dataset_scoring_test

from rsmi import feature_ranking
import joblib

def test():

	dataset_path = 'test/samples/Dataset.pkl'
	
	n_folds  = 2
	
	datasetObject = joblib.load(dataset_path) #load dataset

	univar_ranks = feature_ranking.univariate_ranking(datasetObject, n_folds, score = False)	#scoring univariate
	
	set_ranks  = feature_ranking.set_ranking(datasetObject, n_folds, score = False)#scoring 
	
	
	print('')
	print('univariate ranking : ', univar_ranks )
	
	print('')
	print('set ranking : ', set_ranks )