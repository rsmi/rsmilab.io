#image_to_classify_test

from rsmi import image_sampler
from rsmi import filters

from rsmi import utils
import os
import cv2

def test():

	input_image_path = 'test/samples/woman.jpg'
	
	filters_method = ['laplacian','sobel','prewitt','scharr', 'minimum', 'maximum', 'mean', 'sd' ]
			
	filters_method2 = ['Grayscale','H_channel','S_channel','V_channel']
	
	ksize = 3
	
	print('')	
	print("image_to_classify_test step 1: create a first instance (imageObj1) of image_to_classify object")
	print("with ['laplacian','sobel','prewitt','scharr', 'minimum', 'maximum', 'mean', 'sd' ] ")
	imageObj1 = image_sampler.image_to_classify(filters,filters_method)
	imageObj1.read_and_filter(os.path.abspath(input_image_path),filters, ksize)
	print('object 1 memory :',imageObj1 )
	
	print('')	
	print("image_to_classify_test step 2: create a second instance (imageObj2) of image_to_classify object")
	print("with ['Grayscale','H_channel','S_channel','V_channel'] ")
	imageObj2 = image_sampler.image_to_classify(filters,filters_method2)
	imageObj2.read_and_filter(os.path.abspath(input_image_path),filters, ksize)
	print('object 2 memory :',imageObj2 )

	
	
	cv2.imshow('imageObj1: Original',imageObj1.image)
	cv2.imshow('imageObj2: Grayscale',imageObj2.feature_Grayscale)
	cv2.imshow('imageObj2: Hue',imageObj2.feature_H_channel)
	cv2.imshow('imageObj1: Maximum (ksize:'+str(ksize) +')',imageObj1.feature_maximum)
	cv2.imshow('imageObj1: Sobel (ksize:'+str(ksize) +')',imageObj1.feature_sobel)
	cv2.imshow('imageObj1: Prewitt (ksize:'+str(ksize) +')',imageObj1.feature_prewitt)
	cv2.imshow('imageObj1: Laplacian (ksize:'+str(ksize) +')',imageObj1.feature_laplacian)	
	
	cv2.waitKey(1000)
	cv2.destroyAllWindows()
	
			
