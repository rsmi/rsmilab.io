#Train_and_save test

import joblib
import os

import sklearn
from sklearn import linear_model
from sklearn import ensemble

def test():

	dataset_path = 'test/samples/Dataset.pkl'
	
	savepath = 'test/samples/classif.pkl'

	dataset = joblib.load(dataset_path) #load
	
	#set classifier
	base_logistic_classifier = sklearn.linear_model.LogisticRegression(penalty='l2', dual=False, C=1,
																		fit_intercept=True, intercept_scaling=1.0,
																		class_weight=None, solver='liblinear',
																		max_iter=1000, n_jobs=-1)

	Boosted_classifier = sklearn.ensemble.AdaBoostClassifier(base_estimator = base_logistic_classifier,n_estimators = 2,
															learning_rate = 0.9 , algorithm = 'SAMME')

	
	Boosted_classifier.fit(dataset.data , dataset.target) # train classifier

	print('')
	print('Trained Classifier mean accuracy : ', Boosted_classifier.score(dataset.data , dataset.target))
	
	#save classifier
	joblib.dump(Boosted_classifier, os.path.abspath(savepath)) 
	print('')
	print('Trained Classifier pickled at : ', os.path.abspath(savepath))
