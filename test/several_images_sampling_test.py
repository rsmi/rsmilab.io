#several_images_sampling_test
#subsampling_test
from rsmi import image_sampler
import custom_filters
from rsmi import utils

import os
import cv2
import joblib



def test():

	input_images_path = ['test/samples/grxA2_T5_L5_16.12.14_122234_2_postic.jpg', 'test/samples/grxA2_T2_L1_10.02.15_102118_5_postic.jpg']
	
	target_images_path = ['test/samples/grxA2_T5_L5_16.12.14_122234_2_postic.zip', 'test/samples/grxA2_T2_L1_10.02.15_102118_5_postic.zip']
	
	dataset_generated_path = 'test/samples/Dataset.pkl'

	filter_module = custom_filters

	filters_method= ['H_channel','gray_sd','gray_maximum']

	ksize = 3
	
	n_samples = 50 # ask for 10 pixels of each class (20pixels)

	Sample_ratio = 0.5 # each image gives 50% of the datzset

	classes_names = ['root','soil']
	
	target_fullpath  = [os.path.abspath(path) for path in target_images_path] 

	input_fullpath  = [os.path.abspath(path) for path in input_images_path]


	image_to_classify_object = image_sampler.image_to_classify(filter_module,filters_method) # initialize an image_to_classify_object

	image_to_classify_object.set_method_dict_default_value(filter_module,ksize)# initialize its default value dictionnay with ksize


	print('')
	print('Subsampling test : ')
	print('-------------------')
	print('number of expected pixels sampled : ', 2*n_samples)
	print('')

	subsamplerobj = image_sampler.bunch_sample_constructor(input_fullpath, target_fullpath, image_to_classify_object, 
														   filter_module, classes_names, n_samples, Sample_ratio)
	

	print('dataset generated :')
	print(subsamplerobj.data )
	print('')
	print('target generated :')
	print(subsamplerobj.target )
	print('')
	print('classes names generated :')
	print(subsamplerobj.target_names )
	print('')
	print('features generated :')
	print(subsamplerobj.feature_names )
	print('')
	print('samples generated :')
	print(subsamplerobj.count )
	
	joblib.dump(subsamplerobj, os.path.abspath(dataset_generated_path)) 
	print('')
	print('dataset pickled at : ', os.path.abspath(dataset_generated_path))

	





