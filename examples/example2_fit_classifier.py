#Train a classifier example

from sklearn.ensemble import RandomForestClassifier

import joblib
import os


##Set filename of pickled database, and of the future classifier pickled
#
dataset_loaded = 'database_ex1.pkl'

classifier_pickled = 'trained_clf_ex2.pkl'

##Create a random forest classifier from sklearn
#
randomforest_classifier = RandomForestClassifier()

##end of user input



script_dir = os.path.dirname(__file__) #absolute dir of this script

dataset_loaded_filepath = os.path.abspath(os.path.join(script_dir,dataset_loaded))

clf_pickled_filepath = os.path.abspath(os.path.join(script_dir,classifier_pickled))


subsamplerobj = joblib.load(dataset_loaded_filepath) # load dataset

X = subsamplerobj.data

y = subsamplerobj.target

randomforest_classifier.fit(X,y)

joblib.dump(randomforest_classifier,clf_pickled_filepath)
