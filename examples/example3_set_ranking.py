#rank features

from rsmi import utils
from rsmi import feature_ranking

import os
import joblib


##Set filename of pickled database
#
dataset_loaded = 'database_ex1.pkl'

csv_file = 'set_ranks_ex3.csv'

n_folds = 3 # number of cross-validation folds

script_dir = os.path.dirname(__file__) #absolute dir of this script

dataset_loaded_filepath = os.path.abspath(os.path.join(script_dir,dataset_loaded))

csv_rank_file_path = os.path.abspath(os.path.join(script_dir,csv_file))



subsamplerobj = joblib.load(dataset_loaded_filepath) # load dataset


print('Initiate feature ranking')


rank_dictionnary = feature_ranking.set_ranking(subsamplerobj,  n_folds, score = True)#univariate ranking 

utils.dict_to_csv(csv_rank_file_path, rank_dictionnary)#save to csv

print('Example 3: Stability ranking done')