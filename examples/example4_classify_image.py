#Classify an image

from rsmi.classification import image_classifier
from rsmi.image_sampler import image_to_classify
from rsmi import filters

import os
import joblib
import numpy
import cv2

#//////////////////////////////////////////////////
images_path_full_list = [ 	'/media/NAS_PHENOME/ADMIRALdata/original_minirhizotron_images/grxA2_T12_L4_04.12.14_150902_1_postic.JPG',
	'/media/NAS_PHENOME/ADMIRALdata/original_minirhizotron_images/grxA2_T1_L5_03.04.15_134743_7_postic.JPG',
	'/media/NAS_PHENOME/ADMIRALdata/original_minirhizotron_images/grxA2_T19_L5_20.05.15_160252_10_postic.JPG',
	'/media/NAS_PHENOME/ADMIRALdata/original_minirhizotron_images/grxA2_T6_L2_07.01.15_164311_3_postic.JPG',
	'/media/NAS_PHENOME/ADMIRALdata/original_minirhizotron_images/grxA2_T5_L5_03.04.15_144105_7_postic.JPG']
#/////////////////////////////////////////////////////

#/////////////////////////////////////////////////////
output_path = '/media/NAS_PHENOME/ADMIRALdata/rsmi_result/'
#/////////////////////////////////////////////////////

#////////////
thres =0.975
#////////////

#Set the method used, as found in example1
#
method_list  = ['Grayscale',
				'R_channel','G_channel','B_channel',
				'H_channel','S_channel','V_channel',
				'Rnorm_channel','Gnorm_channel','Bnorm_channel']

#Initialize an instance of image_to_classify_object, that will contains the original image and the features
#
image_to_classify_object =	image_to_classify(filters,method_list)

#Load a trained classifier with joblib
#
classifier_pickled = 'trained_clf_ex2.pkl'

clf_pickled_filepath = os.path.abspath(os.path.join(script_dir,classifier_pickled))

randomforest_classifier = joblib.load(clf_pickled_filepath)


#Create the features from the filtering methods listed in 'method_list'
#
image_to_classify_object.read_and_filter(image_path,custom_filters)

#Create a image_classifier object, that takes a image_to_classify object and applies a classifier
#
image_classifier_object = image_classifier()

#Here, we choose to get the class predictions with a threshold equal to 'thresh'
#
image_classifier_object.predict(image_to_classify_object, randomforest_classifier, threshold = thres)


#Write the predicted image in a file
#
cv2.imwrite(output_path + 'Logit_8feat_' + basename,  image_classifier_object.image_classified)

