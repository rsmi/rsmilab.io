#Classifier ranking

#-*- coding: utf-8 -*-
from rsmi import feature_ranking
from rsmi import utils

#Let's import some classifiers from the scikit learn
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier


from sklearn.model_selection import train_test_split

import joblib
import os
import numpy


##Instantiation of different classifiers with their default options
#
Naive_bayes_classifier = GaussianNB()#Naive Bayes (classifier on continuous features)

LinSVM_classifier = LinearSVC()#Linear SVM

randomforest_classifier = RandomForestClassifier()#Random forest

##Set the list of the classifiers used
#
classif_list = [ Naive_bayes_classifier, LinSVM_classifier, randomforest_classifier ]#First, the list of object instances

classif_names = ['Naive Bayes', 'Linear SVM','Random Forest']#Then, their name in the dictionary

##Set filename of pickled database
#
dataset_loaded = 'database_ex1.pkl'

##Set the ranking parameters
#
nfolds = 3 # number of folds for cross-validation

features_ranks = [1,2,3,4,5,6,7,8,9,10] #

nSample = 50000#

nbins = 20 #

##Set filename of the saved F0.5 score table
#
score_table = 'clf_ranks_ex5.csv'


script_dir = os.path.dirname(__file__) #absolute dir of this script

csv_rank_file_path = os.path.abspath(os.path.join(script_dir,score_table))



##Load the dataset
#
script_dir = os.path.dirname(__file__) #absolute dir of this script

dataset_loaded_filepath = os.path.abspath(os.path.join(script_dir,dataset_loaded))


X = subsamplerobj.data

y = subsamplerobj.target
	
n_observation = len(y)#get the size of the whole dataset



## Draw a random subset of the dataset
#
split_size = nSample/n_observation#Set the percentage of the data sampled

X_dump, X_sampled, y_dump, y_sampled  = train_test_split(X, y, test_size=split_size, random_state=None, stratify = y)


##Compute the F0.5 score for a varying number of feature used
#
Classif_rank_dictionnary_F05 = feature_ranking.classif_F05_score(classif_list, classif_names, X_sampled, y_sampled, nfolds, features_ranks, nbins = nbins  ) 

utils.dict_to_csv(save_path , Classif_rank_dictionnary_F05)#save to csv






