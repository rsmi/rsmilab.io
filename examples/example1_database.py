#create database example

from rsmi import image_sampler
from rsmi import filters

import joblib
import os

## set directories names
#

original_dir_name = 'original/'

groundtruth_dir_name = 'groundtruth/'

dataset_generated = 'database_ex1.pkl'

##Set the sampling characteristics
#
n_samples = 1e4 # ask for 10 000  pixels of each class

Sample_ratio = 0.10 # each image gives 10% of the dataset

classes_names = ['root','soil']

##Set the image filtering methods used here (found in the 'filters' module)
#
method_list  = ['Grayscale',
				'R_channel','G_channel','B_channel',
				'H_channel','S_channel','V_channel',
				'Rnorm_channel','Gnorm_channel','Bnorm_channel']
				

##end of user input

##Set images used for pixel sampling
##
script_dir = os.path.dirname(__file__) #absolute dir of this script

original_dir = os.path.join(script_dir, original_dir_name)

groundtruth_dir = os.path.join(script_dir, groundtruth_dir_name)

original_path = [os.path.join(original_dir, f) for f in os.listdir(original_dir)]

groundtruth_path = [os.path.join(groundtruth_dir, f) for f in os.listdir(groundtruth_dir)]


##Set path of the dataset generated
#
dataset_generated_filepath = os.path.abspath(os.path.join(script_dir,dataset_generated))


##Create an instance of the image_to_classify object, with 'filters' module and 'method_list' list of image processing filters applied
#				
image_to_classify_object = image_sampler.image_to_classify(filters,method_list) # initialize an image_to_classify_object
image_to_classify_object.set_method_dict_default_value(filters)
	
## Call the sampling method via instanciation of a image_sampler.bunch_sample_constructor object
#
subsamplerobj = image_sampler.bunch_sample_constructor(original_path, groundtruth_path, image_to_classify_object, 
														   filters, classes_names, n_samples, Sample_ratio)				
														   
##Save the dataset into a pickle
#
joblib.dump(subsamplerobj, dataset_generated_filepath) # save datasets	   

